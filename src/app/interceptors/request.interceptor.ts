import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { AuthStorageService } from 'src/app/services/auth-storage/auth-storage.service';
import { UserDataStorageService } from 'src/app/services/user-storage/user-data-storage.service';

@Injectable()
// Class de conditionnement des header HTTP pour l'envoi de cookie (XSRF ou JWT)
export class XhrInterceptor implements HttpInterceptor {

  constructor(private authStorageService: AuthStorageService,
              private userDataStorageService: UserDataStorageService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {

    ///////////////////////////////HEADER PACKAGING///////////////////////
    let httpHeaders = new HttpHeaders();
    httpHeaders = httpHeaders.set('Content-Type', 'application/json');

    // user connection requested
    if (req.url.includes('/customer') && req.method === 'GET') {
      const email = this.authStorageService.getEmail();
      const password = this.authStorageService.getPassword();
      if (email && password) {
        const basicAuth = 'Basic ' + window.btoa(email + ':' + password);
        httpHeaders = httpHeaders.set('Authorization', basicAuth);
        //console.log("Add Basic Authentication in HTTP header");
      }
    }
    // other requests protected
    else {
      const authorization = this.userDataStorageService.getUserData()?.userSecurity.Authorization;
      if(authorization){
        httpHeaders = httpHeaders.set('Authorization', authorization);
        //console.log('Add JWT in HTTP header');
      } else {
        //console.warn('JWT not found in userDataStorageService');
      }
      const xsrf = this.userDataStorageService.getUserData()?.userSecurity.XSRF_TOKEN;
      if(xsrf){
        httpHeaders = httpHeaders.set('X-CSRF-TOKEN', xsrf);
        //console.log('XSRF-TOKEN add in HTTP header', xsrf);
      } else {
        //console.warn('XSRF-TOKEN not found in userDataStorageService');
      }
    }

    httpHeaders = httpHeaders.set('X-Requested-With', 'XMLHttpRequest');

    const xhr = req.clone({
      headers: httpHeaders
    });

    return next.handle(xhr).pipe(tap(
      (event: any) => {
        // logique de traitement des réponses ici
      },
      (err: any) => {
        if (err instanceof HttpErrorResponse) {
          console.error('HTTP Error:', err.message);
        }
      }
    ));
  }
}
