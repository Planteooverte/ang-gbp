import { Directive, HostBinding, Input } from '@angular/core';
import { UserService } from '../services/user/user.service';
import { Subscription } from 'rxjs';

@Directive({
  selector: '[appLinespacing]'
})
export class LinespacingDirective {
  @Input() appLinespacing: string | undefined;

  // Tableau clef-valeur d'association des options de contraste à une classe CSS
  lineSpacingStyles: { [key: string]: string } = {
    'Défaut': 'default-linespacing',
    'Augmenté': 'adapted-linespacing',
  };

  //Propriété pour stocker l'abonnemenet à l'observable
  private subscription: Subscription | undefined;

  constructor(private userService: UserService) {}

  @HostBinding('class') get elementClass() {
    return this.appLinespacing || 'default-contrast';
  }

  ngAfterContentChecked () {
    //Souscription à l'observable userService pour récupérer les préférences de l'utilisateur
    this.subscription = this.userService.userPreferences$.subscribe(
      () => {
        // Utilisation de la méthode getUserPreferences pour obtenir les préférences de l'utilisateur
        const preferences = this.userService.getUserPreferences();

        //Récupération du style CSS associé aux préférences de l'utilisateur et activation sur l'interface utilisateur
        const selectedStyle = this.lineSpacingStyles[preferences.lineSpacing];
        this.appLinespacing = selectedStyle;
      }
    );
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
