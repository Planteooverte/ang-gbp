import { Directive, HostBinding, Input } from '@angular/core';
import { UserService } from '../services/user/user.service';
import { Subscription } from 'rxjs';

@Directive({
  selector: '[appFont]'
})
export class FontDirective {
  @Input() appFont: string | undefined;

  // Tableau clef-valeur d'association des options de contraste à une classe CSS
  fontStyles: { [key: string]: string } = {
    'Défaut': 'default-font',
    'Adapté': 'reinforced-font',
  };

  //Propriété pour stocker l'abonnemenet à l'observable
  private subscription: Subscription | undefined;

  constructor(private userService: UserService) {}

  @HostBinding('class') get elementClass() {
    return this.appFont || 'default-font';
  }

  ngAfterContentChecked () {
    //Souscription à l'observable userService pour récupérer les préférences de l'utilisateur
    this.subscription = this.userService.userPreferences$.subscribe(
      () => {
        // Utilisation de la méthode getUserPreferences pour obtenir les préférences de l'utilisateur
        const preferences = this.userService.getUserPreferences();

        //Récupération du style CSS associé aux préférences de l'utilisateur et activation sur l'interface utilisateur
        const selectedStyle = this.fontStyles[preferences.font];
        this.appFont = selectedStyle;
      }
    );
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
