import { Directive, HostBinding, Input, ElementRef } from '@angular/core';
import { UserService } from '../services/user/user.service';
import { Subscription } from 'rxjs';

@Directive({
  selector: '[appContrast]'
})
export class ContrastDirective {
  @Input() appContrast: string | undefined;

  // Tableau clef-valeur d'association des options de contraste à une classe CSS
  contrastStyles: { [key: string]: string } = {
    'Défaut': 'default-contrast',
    'Renforcé': 'reinforced-contrast',
    'Inversé': 'reversed-contrast',
  };

  //Propriété pour stocker l'abonnemenet à l'observable
  private subscription: Subscription | undefined;

  constructor(private userService: UserService) {}

  @HostBinding('class') get elementClass() {
    return this.appContrast || 'default-contrast';
  }

  ngAfterContentChecked () {
    //Souscription à l'observable userService pour récupérer les préférences de l'utilisateur
    this.subscription = this.userService.userPreferences$.subscribe(
      () => {
        // Utilisation de la méthode getUserPreferences pour obtenir les préférences de l'utilisateur
        const preferences = this.userService.getUserPreferences();

        //Récupération du style CSS associé aux préférences de l'utilisateur et activation sur l'interface utilisateur
        const selectedStyle = this.contrastStyles[preferences.contrast];
        this.appContrast = selectedStyle;
      }
    );
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
