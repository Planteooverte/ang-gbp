//Native angular
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

//Components
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { FormLoginComponent } from './components/form-login/form-login.component';
import { FormContactComponent } from './components/form-contact/form-contact.component';
import { CguComponent } from './components/cgu/cgu.component';
import { XhrInterceptor } from './interceptors/request.interceptor';
import { AuthGuard } from './services/auth-guard/auth.guard';
import { BankStatementComponent } from './pages/bank-statement/bank-statement.component';
import { FormDisabledAccessComponent } from './components/form-disabled-access/form-disabled-access.component';
import { FormForgottenPwdComponent } from './components/form-forgotten-pwd/form-forgotten-pwd.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { SettingsComponent } from './pages/settings/settings.component';
import { PresentationComponent } from './pages/presentation/presentation.component';
import { BankAccountComponent } from './components/bank-account/bank-account.component';
import { FormsBankAccountComponent } from './components/bank-account/forms-bank-account/forms-bank-account.component';
import { CsvfileComponent } from './components/csvfile/csvfile.component';
import { FormsCsvfileComponent } from './components/csvfile/forms-csvfile/forms-csvfile.component';
import { BankingTransactionComponent } from './components/banking-transaction/banking-transaction.component';
import { StatisticalComponent } from './components/statistical/statistical.component';
import { FormRegisterComponent } from './components/form-register/form-register.component';
import { ProfileComponent } from './components/profile/profile.component';
import { DisconnectComponent } from './components/header/disconnect/disconnect.component';
import { FormsProfileComponent } from './components/profile/forms-profile/forms-profile.component';

//Material dependancies
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatMenuModule } from '@angular/material/menu';
import { MatDialogModule } from '@angular/material/dialog';
import { MatRadioModule } from '@angular/material/radio';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatListModule } from '@angular/material/list';
import { MatDividerModule } from '@angular/material/divider';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSelectModule } from '@angular/material/select';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MaterialFileInputModule } from 'ngx-material-file-input';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatStepperModule } from '@angular/material/stepper';

//Directive
import { ContrastDirective } from './directives/contrast.directive';
import { FontDirective } from './directives/font.directive';
import { LinespacingDirective } from './directives/linespacing.directive';
import { DragAndDropDirective } from './directives/drag-and-drop.directive';

//HTTP
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClientXsrfModule } from '@angular/common/http';

//Highcharts
import { HighchartsChartModule } from 'highcharts-angular';
import { SemiCircleChartDebitComponent } from './components/charts/semi-circle-chart-debit/semi-circle-chart-debit.component';
import { ColumnChartComponent } from './components/charts/column-chart/column-chart.component';
import { BasicAreaChartComponent } from './components/charts/basic-area-chart/basic-area-chart.component';
import { SemiCircleChartCreditComponent } from './components/charts/semi-circle-chart-credit/semi-circle-chart-credit.component';



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    FormLoginComponent,
    FormContactComponent,
    CguComponent,
    FormDisabledAccessComponent,
    ContrastDirective,
    FontDirective,
    LinespacingDirective,
    DashboardComponent,
    SemiCircleChartDebitComponent,
    ColumnChartComponent,
    BasicAreaChartComponent,
    SettingsComponent,
    BankStatementComponent,
    PresentationComponent,
    SemiCircleChartCreditComponent,
    BankAccountComponent,
    FormsBankAccountComponent,
    CsvfileComponent,
    FormsCsvfileComponent,
    DragAndDropDirective,
    BankingTransactionComponent,
    StatisticalComponent,
    FormRegisterComponent,
    FormForgottenPwdComponent,
    ProfileComponent,
    DisconnectComponent,
    FormsProfileComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    HttpClientXsrfModule,
    MatIconModule,
    MatButtonModule,
    MatToolbarModule,
    MatMenuModule,
    MatDialogModule,
    MatRadioModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    HighchartsChartModule,
    MatCardModule,
    MatExpansionModule,
    MatListModule,
    MatDividerModule,
    MatTableModule,
    MatTooltipModule,
    CommonModule,
    MatSelectModule,
    MatPaginatorModule,
    MatSortModule,
    MaterialFileInputModule,
    MatCheckboxModule,
    MatStepperModule,
  ],
  exports: [
    DragAndDropDirective,
  ],
  providers: [
    {
      provide : HTTP_INTERCEPTORS,
      useClass : XhrInterceptor,
      multi : true
    },
    AuthGuard,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
