import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserDataStorageService } from 'src/app/services/user-storage/user-data-storage.service';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
})

export class DashboardComponent{
  //Declaration of variables
  isAuthorized: boolean | undefined;

  constructor(private userDataStorageService: UserDataStorageService,
              private router: Router){
  };

  ngOnInit(){
    const userData = this.userDataStorageService.getUserData();
    //Check User authenticated
    if (userData?.userSecurity.isAuthorized) {
      console.log('User is authorized');
    } else {
      this.router.navigate(['welcome']);
    }
  }
}
