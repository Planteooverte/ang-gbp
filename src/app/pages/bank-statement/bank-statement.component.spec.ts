import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageBankStatementComponent } from './bank-statement.component';

describe('PageBankStatementComponent', () => {
  let component: PageBankStatementComponent;
  let fixture: ComponentFixture<PageBankStatementComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PageBankStatementComponent]
    });
    fixture = TestBed.createComponent(PageBankStatementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
