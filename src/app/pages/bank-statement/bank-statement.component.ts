import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserDataStorageService } from 'src/app/services/user-storage/user-data-storage.service';

@Component({
  selector: 'app-bank-statement',
  templateUrl: './bank-statement.component.html',
  styleUrls: ['./bank-statement.component.scss'],
})

export class BankStatementComponent implements OnInit {

  constructor(private userDataStorageService: UserDataStorageService,
              private router: Router){
  };

  ngOnInit(){
    const userData = this.userDataStorageService.getUserData();
    //Check User authenticated
    if (userData?.userSecurity.isAuthorized) {
      console.log('User is authorized');
    } else {
      console.log('User is not authorized or user data not found!');
      this.router.navigate(['welcome']);
    }
  }
}
