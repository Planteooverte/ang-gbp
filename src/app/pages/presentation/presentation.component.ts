import { Component, OnInit } from '@angular/core';
import { userPreferenceInterface } from 'src/app/models/userSession';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-presentation',
  templateUrl: './presentation.component.html',
  styleUrls: ['./presentation.component.scss'],
})
export class PresentationComponent {

  userPreferences: userPreferenceInterface = {
    contrast: 'Défaut',
    font: 'Défaut',
    lineSpacing: 'Défaut'
  };

  constructor(private userService: UserService) {}

  ngOnInit() {
    // Souscrire aux préférences utilisateur
    this.userService.userPreferences$.subscribe(preferences => {
      this.userPreferences = preferences;
      console.log("User preference:", preferences)
    });
  }
}
