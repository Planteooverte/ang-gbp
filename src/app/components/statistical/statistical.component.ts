import { Component, OnInit } from '@angular/core';
import { FinancialRequestService } from 'src/app/services/financial-request/financial-request.service';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { BankAccount, TypeAccount } from 'src/app/models/bankAccount';
import { StatisticalResponse } from 'src/app/models/statisticalResponse';
import { BalanceTampingStatDTO } from 'src/app/models/balanceTampingStatDTO';
import { PercentageCatStatDTO } from 'src/app/models/percentageCatStatDTO';
import { StatBankTransaction } from 'src/app/models/statBankTransaction';
import { Subscription, take } from 'rxjs';
import { NbTransactionStatDTO } from 'src/app/models/nbTransactionStatDTO';
import { StatusBankingTransaction, StatusTransaction, TransactionsOnly } from 'src/app/models/statusBankingTransaction';
import { UserDataStorageService } from 'src/app/services/user-storage/user-data-storage.service';


@Component({
  selector: 'app-statistical',
  templateUrl: './statistical.component.html',
  styleUrls: ['./statistical.component.scss']
})

export class StatisticalComponent implements OnInit{

  //Déclaration des variables
  listBankAccount: BankAccount[] | undefined;
  listUniquePeriods: string[] = [];
  private subscriptions: Subscription[] = [];
  selectedBankAccountId: number | 'all' = 'all';
  selectedPeriodSlide1: string = 'all';
  selectedPeriodSlide2: string = 'all';
  balanceTampingStatDTOs: BalanceTampingStatDTO[]= [];
  percentageCatStatDTOs: PercentageCatStatDTO[]= [];
  nbTransactionStatDTOs : NbTransactionStatDTO[]= [];
  statBankTransaction = new StatBankTransaction();
  dataForCreditSemiCircleChart: any[] = [];
  dataForDebitSemiCircleChart: any[] = [];
  dataForBasicAreaChart: any[] = [];
  currentSlide = 0;
  errormessage: string | undefined;
  error = false;
  userId: number = 0;

  constructor(private financialRequestService: FinancialRequestService,
              private userDataStorageService: UserDataStorageService){
              };

  ngOnInit(){
    // Subscription to Observable to obtain user data
    const userData = this.userDataStorageService.getUserData();
    if (userData && userData.userSecurity.isAuthorized) {
      this.userId = userData.userProfil.id;
    } else {
      console.error("User is not authorized or user data not found!");
    }


    //Loading the bankaccount drop-down menu
    this.loadBankAccountByUserId(this.userId);
  }

  //////////////////////////////////////DROP-DOWN MENU FOR LIST BANKACCOUNT////////////////////////
  loadBankAccountByUserId(id: number){
    const sub = this.financialRequestService.getShortListBankAccountsByUserId(id).pipe(take(1)).subscribe({
      next: (response: HttpResponse<any>) => {
        console.log("Request approved: <-------", response);
        const dtoListBankAccounts: BankAccount[] = <BankAccount[]>response.body;
        this.listBankAccount = dtoListBankAccounts;
        console.log("Objet received - bankAccount DTO", this.listBankAccount);
        this.userDataStorageService.updateXSRFToken(response.headers.get('X-CSRF-TOKEN') || '');
      },
      error: (error: HttpErrorResponse) => {
        console.log("Request rejected: <-------", error);
        this.error = true;
          if (error.status === 404) {
            this.errormessage = `No bank account found`;
          } else {
            this.errormessage = `Error retrieving bank account: ${error.message}`;
          }
        this.userDataStorageService.updateXSRFToken(error.headers.get('X-CSRF-TOKEN') || '');
      }
    });
    this.subscriptions.push(sub);
  }

  //////////////////////////////////////CONVERSION METHOD//////////////////////////////////////////
  getTypeAccountLabel(key: string): string {
    return TypeAccount[key as keyof typeof TypeAccount] || '';
  }

  /////////////////////////GET AND DISPLAY STATISCAL OF BANACCOUNT SELECTED////////////////////////
  displayStatOfBankAccountSelected() {
    //Reset statBankTransaction to default values
    this.statBankTransaction = {
      monthDisplay: '',
      periodDisplay: '',
      balanceDisplay: 'N/A',
      monthCreditDisplay: 'N/A',
      monthDebitDisplay: 'N/A',
      filteredBalanceTampingStatDTOs: [],
      filteredPercentageCatDTOs: [],
      filteredNbTransactionStatDTOs: [],
      serialDataForBasicAreaChart: [],
      serialDataForSemiCircleChartCredit: [],
      serialDataForSemiCircleChartDebit: [],
      listStatusBankingTransaction: [],
      months: [],
      monthMap: {},
    };
    //Reset periods to 'all' and graphs
    this.selectedPeriodSlide1 = 'all';
    this.selectedPeriodSlide2 = 'all';
    this.listUniquePeriods = [];
    this.dataForBasicAreaChart = [];
    this.dataForCreditSemiCircleChart = [];
    this.dataForDebitSemiCircleChart = [];

    console.log("Retrieve statistics for bank account:", this.selectedBankAccountId);

    if (this.selectedBankAccountId !== 'all'){
      const sub = this.financialRequestService.getStatisticalOnBankAccount(this.selectedBankAccountId).pipe(take(1)).subscribe(
        response => {
          if (response.body !== null){

            //Assigning data to variables
            const responseData: StatisticalResponse = response.body;
            this.balanceTampingStatDTOs = responseData.balanceTampingStatDTOs;
            this.percentageCatStatDTOs = responseData.percentageCatStatDTOs;
            this.nbTransactionStatDTOs = responseData.nbTransactionStatDTOs;

            //Display data in the console for verification
            console.log('Balance Tamping Stat DTOs:', this.balanceTampingStatDTOs);
            console.log('Percentage Cat DTOs:', this.percentageCatStatDTOs);
            console.log( 'Number of Transaction DTOs', this.nbTransactionStatDTOs);

            // Mettre à jour le menu déroulant 'période' en fonction du slide
            this.updatePeriodDropdown();

            // Reset error section
            this.error = false;

          } else {
            console.warn('No data received.');
          }

          this.userDataStorageService.updateXSRFToken(response.headers.get('X-CSRF-TOKEN') || '');
        },
          error => {
          this.error = true;
          this.userDataStorageService.updateXSRFToken(error.headers.get('X-CSRF-TOKEN') || '');
          if (error.status === 404) {
            this.errormessage = `No banking transactions found`;
          } else {
            this.errormessage = `Error retrieving banking transactions: ${error.message}`;
          }
          //Managing query errors
          console.error('Error when retrieving statistics:', error);
        }
      );
      this.subscriptions.push(sub);
    }
  }

  ////////////////////////////////////DATA PREPARATION FOR USER INTERFACE////////////////////
  loadStatisticalResponseWithFilter() {
    // Déclaration des variables
    let periodFilter = this.selectedPeriodSlide1;
    if (this.currentSlide === 1) {
      periodFilter = this.selectedPeriodSlide2;
    }

    // Reset des données si la période est "all"
    if (periodFilter === 'all') {
      //Reset statBankTransaction to default values
      this.statBankTransaction = {
        monthDisplay: '',
        periodDisplay: '',
        balanceDisplay: 'N/A',
        monthCreditDisplay: 'N/A',
        monthDebitDisplay: 'N/A',
        filteredBalanceTampingStatDTOs: [],
        filteredPercentageCatDTOs: [],
        filteredNbTransactionStatDTOs: [],
        serialDataForBasicAreaChart: [],
        serialDataForSemiCircleChartCredit: [],
        serialDataForSemiCircleChartDebit: [],
        listStatusBankingTransaction: [],
        months: [],
        monthMap: {},
      };
      //Reset graphs
      this.dataForBasicAreaChart = [];
      this.dataForCreditSemiCircleChart = [];
      this.dataForDebitSemiCircleChart = [];
      return;
    }

    //Section SLIDE 1
    if (this.currentSlide === 0) {
      //Variables declaration
      const [selectedMonth, selectedYear] = periodFilter.split('/').map(Number);

      //Filter balanceTampingStatDTOs data by year
      const filteredBalanceTampingStatDTOs = this.balanceTampingStatDTOs.filter(dto => {
        const period = `${dto.year}`;
        return period === selectedYear.toString();
      });

      //Filter percentageCatDTOs data by year and month
      const filteredPercentageCatDTOs = this.percentageCatStatDTOs.filter(dto => {
        const period = `${dto.month}/${dto.year}`;
        return period === periodFilter;
      });

      //Preparing data for the BasicAreaChart graph
      let SerialDataForBasicAreaChart: number[] = [];
      let positiontab = 0;
      const nbligne = filteredBalanceTampingStatDTOs.length;

      while (positiontab < nbligne) {
        const tampingBalance = filteredBalanceTampingStatDTOs[positiontab].tampingBalance;
        SerialDataForBasicAreaChart.push(parseFloat(tampingBalance.toFixed(2)));
        positiontab++;
      }

      //Preparing data for the SemiCircleChartCredit and SemiCircleChartDebit graphs
      let SerialDataForSemiCircleChartCredit: [string, number][] = [];
      let SerialDataForSemiCircleChartDebit: [string, number][] = [];
      positiontab = 0;
      const nblignePercentageCat = filteredPercentageCatDTOs.length;

      while (positiontab < nblignePercentageCat) {
        const { categoryName, percentTotalCatCredit, percentTotalCatDebit } = filteredPercentageCatDTOs[positiontab];
        const capitalizedCategoryName = this.capitalizeFirstLetter(categoryName);
        if (percentTotalCatCredit !== null) {
        SerialDataForSemiCircleChartCredit.push([capitalizedCategoryName, parseFloat(percentTotalCatCredit.toFixed(2))]);
        }
        if (percentTotalCatDebit !== null) {
          SerialDataForSemiCircleChartDebit.push([capitalizedCategoryName, parseFloat(percentTotalCatDebit.toFixed(2))]);
        }
        positiontab++;
      }

      //Update of the period displayed in the 'Solde du compte' KPI
      this.statBankTransaction.periodDisplay = `${this.getStringMonth(selectedMonth)}/${selectedYear}`;

      //Extraction KPI - solde du compte, total débit et total crédit
      const kpiData = filteredBalanceTampingStatDTOs.find(dto => dto.month === selectedMonth && dto.year === selectedYear);
      const tampingBalance = kpiData ? kpiData.tampingBalance.toFixed(2) : 'N/A';
      const totalDebit = kpiData ? kpiData.totalDebit.toFixed(2) : 'N/A';
      const totalCredit = kpiData ? kpiData.totalCredit.toFixed(2) : 'N/A';

      //Updating data for graphics
      this.dataForBasicAreaChart = SerialDataForBasicAreaChart;
      this.dataForCreditSemiCircleChart = SerialDataForSemiCircleChartCredit;
      this.dataForDebitSemiCircleChart = SerialDataForSemiCircleChartDebit;

      //Saving the statistics displayed
      this.statBankTransaction.balanceDisplay = tampingBalance;
      this.statBankTransaction.monthDebitDisplay = totalDebit;
      this.statBankTransaction.monthCreditDisplay = totalCredit;
      this.statBankTransaction.monthDisplay = this.getStringMonth(selectedMonth);
      this.statBankTransaction.serialDataForBasicAreaChart = SerialDataForBasicAreaChart;
      this.statBankTransaction.serialDataForSemiCircleChartCredit = SerialDataForSemiCircleChartCredit;
      this.statBankTransaction.serialDataForSemiCircleChartDebit = SerialDataForSemiCircleChartDebit;
      this.statBankTransaction.filteredBalanceTampingStatDTOs = filteredBalanceTampingStatDTOs;
      this.statBankTransaction.filteredPercentageCatDTOs = filteredPercentageCatDTOs;
    }

    //Section SLIDE 2
    else if (this.currentSlide === 1) {
      //Variables declaration
      const selectedYear = Number(periodFilter);
      const months: string[] = [];
      const monthMap: { [key: string]: number } = {};
      const statusBankingTransaction: StatusBankingTransaction[] = [
        { type: 'status' },
        { type: 'transactions' }
      ];

      //Filter nbTransactionStatDTOs data by year
      const filteredNbTransactionStatDTOs = this.nbTransactionStatDTOs.filter(dto => dto.year === selectedYear);

      //Mapping du mois en string et number
      filteredNbTransactionStatDTOs.forEach(dto => {
        const monthString = this.getStringMonth(dto.month);
        months.push(monthString);
        monthMap[monthString] = dto.month; // Mapping du mois en texte au mois en nombre
      });

      filteredNbTransactionStatDTOs.forEach(dto => {
        const month = this.getStringMonth(dto.month);
        statusBankingTransaction[0][monthMap[month]] = {
          status: dto.attendanceRecord,
          transactions: dto.nbTransactions
        } as StatusTransaction;
        statusBankingTransaction[1][monthMap[month]] = {
          transactions: dto.nbTransactions
        } as TransactionsOnly;
      });

      //Saving the statistics displayed
      this.statBankTransaction.filteredNbTransactionStatDTOs = filteredNbTransactionStatDTOs;
      this.statBankTransaction.listStatusBankingTransaction = statusBankingTransaction;
      this.statBankTransaction.months = months;
      this.statBankTransaction.monthMap = monthMap;
    }

    console.log('statBankTransaction:', this.statBankTransaction);
  }

  //////////////////////////////METHOD TO CAPITALIZE THE FIRST LETTER////////////////////////////
  capitalizeFirstLetter(text: string): string {
    return text.charAt(0).toUpperCase() + text.slice(1).toLowerCase();
  }

  //////////////////////////METHOD TO CONVERT THE NUMBER MONTH INTO A STRING/////////////////////
  getStringMonth(monthNumber: number): string {
    const monthNames = ['Jan', 'Fév', 'Mar', 'Avr', 'Mai', 'Jui', 'Jul', 'Aoû', 'Sep', 'Oct', 'Nov', 'Déc'];
    const monthName = monthNames[monthNumber - 1];
    return monthName;
  }

  /////////////////////////////////////////////CARROUSEL/////////////////////////////////////////
  prevSlide() {
    this.currentSlide = (this.currentSlide === 0) ? 1 : this.currentSlide - 1;
    this.updatePeriodDropdown();
  }

  nextSlide() {
    this.currentSlide = (this.currentSlide === 1) ? 0 : this.currentSlide + 1;
    this.updatePeriodDropdown();
  }

  //////////////////////////////METHODE UPDATE DROP DOWN PERIOD MENU////////////////////////////
  updatePeriodDropdown() {
    if (this.currentSlide === 0) {
      this.listUniquePeriods = Array.from(new Set(this.balanceTampingStatDTOs.map(dto => `${dto.month}/${dto.year}`)));
    } else if (this.currentSlide === 1) {
      this.listUniquePeriods = Array.from(new Set(this.nbTransactionStatDTOs.map(dto => `${dto.year}`)));
    }
  }

}

// # Ce programme est un logiciel libre : vous pouvez le redistribuer et/ou le modifier
// # selon les termes de la Licence Publique Générale GNU publiée par la
// # Free Software Foundation, version 3.

// # Ce programme est distribué dans l'espoir qu'il sera utile,
// # mais SANS AUCUNE GARANTIE ; sans même la garantie implicite de
// # COMMERCIALISATION ou d'ADÉQUATION À UN OBJECTIF PARTICULIER. Voir la
// # Licence Publique Générale GNU pour plus de détails.

// # Vous devriez avoir reçu une copie de la Licence Publique Générale GNU
// # avec ce programme. Si ce n'est pas le cas, voir https://www.gnu.org/licenses/.

// # Conditions supplémentaires :
// # 1. Le logiciel peut être utilisé et modifié uniquement dans un cadre personnel ou éducatif (enseignement, apprentissage).
// # 2. Toute redistribution du logiciel dans un but professionnel est interdite.
// # 3. La commercialisation du logiciel est interdite sans l'accord explicite de l'auteur.
// # 4. Toute version modifiée du logiciel utilisée dans un cadre commercial doit rendre le code source disponible.
