import { Component, Inject, Output, EventEmitter } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AbstractControl, ValidationErrors } from '@angular/forms';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { CustomerRequestService } from 'src/app/services/customer-request/customer-request.service';
import { UserDataStorageService } from 'src/app/services/user-storage/user-data-storage.service';
import { interval, Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { RegistrationData } from 'src/app/models/registrationData';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';


@Component({
  selector: 'app-forms-profile',
  templateUrl: './forms-profile.component.html',
  styleUrls: ['./forms-profile.component.scss']
})

export class FormsProfileComponent {
  //Variables declaration
  userData: any;
  action: string;
  profileForm: FormGroup;
  secretCodeForm: FormGroup;
  hide: boolean = true;
  isSecretCodeDisplayed = false;
  typeProcess: string = 'changeEmail';
  private subscriptions: Subscription[] = [];
  successMessage: string = '';
  errorMessage: string = '';
  countdown: number = environment.countdown;
  @Output() formClosed = new EventEmitter<boolean>();

  constructor(  @Inject(MAT_DIALOG_DATA) public data: any,
                private formBuilder: FormBuilder,
                private customerRequestService: CustomerRequestService,
                private userDataStorageService: UserDataStorageService,
                public dialogRef: MatDialogRef<FormsProfileComponent>,
                private router: Router){

    // Recovery of Primary component variables
    this.action = data?.action || 'default';

    // Initialising an Checking values from profileForm
    this.profileForm = this.formBuilder.group({
      id: new FormControl( 0, [
        Validators.required,
      ]),
      lastName: new FormControl( '', [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(30),
      ]),
      firstName: new FormControl('', [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(30),
      ]),
      email: new FormControl( '', [
        Validators.required,
        Validators.email,
        Validators.minLength(6),
        Validators.maxLength(50),
      ]),
      password: new FormControl( '', [
        Validators.required,
        Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*\W).*$/),
        Validators.minLength(8),
        Validators.maxLength(50),
      ]),
      passwordControl: new FormControl( '', [
        Validators.required,
        this.matchValues('password'),
      ]),
    });

    this.secretCodeForm = this.formBuilder.group({
      secretCode: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(6),
      ]),
    });

  }

  ngOnInit(): void {
    // Display user information
    this.userData = this.userDataStorageService.getUserData();

    // Initialise form values with user data
    if (this.userData) {
      this.profileForm.patchValue({
        id: this.userData.userProfil.id || 0,
        lastName: this.userData.userProfil.lastName || '',
        firstName: this.userData.userProfil.firstName || '',
        email: this.userData.userProfil.email || '',
        password: this.userData.userProfil.password || '',
        passwordControl: this.userData.userProfil.password || ''
      });
    }
  }

  //////////////////////////////////BUTTON METHODS///////////////////////////
  processSwitch() {
    const oldEmail = this.userData.userProfil.email;
    const newEmail = this.profileForm.get('email')?.value;

    if (newEmail !== oldEmail) {
      // Email has changed, trigger the secret code request
      this.secretCodeRequest(newEmail);
    } else {
      // Email has not changed, directly update the profile
      this.updateProfilRequest(this.userData.userProfil.id);
    }
  }

  updateWithSecretCode() {
    if (this.secretCodeForm.valid) {
      // Update userProfil with secretCode
      this.updateProfilRequest(this.userData.userProfil.id);
    } else {
      console.log('Le code secret est invalide.');
    }
  }

  closeFormProfile(){
    this.dialogRef.close();
  }

  //////////////////////////////////REQUEST METHODS///////////////////////////
  secretCodeRequest(email: string) {
    if (this.profileForm.get('email')?.value !== email) {
      return;
    }
    // Get secretCode on the new email
    const sub = this.customerRequestService.getSecretCode(email, this.typeProcess).pipe(take(1)).subscribe({
      next: (response) => {
        console.log("Request approved: <-------", response);
        // Switch to the secret code form with new button
        this.action = 'secretcode';
        this.isSecretCodeDisplayed = true;
        this.errorMessage = "";
        this.userDataStorageService.updateXSRFToken(response.headers.get('X-CSRF-TOKEN') || '');
      },
      error: (HttpErrorResponse) => {
        console.log("Request rejected: <-------", HttpErrorResponse);
        this.errorMessage = HttpErrorResponse.message;
        this.userDataStorageService.updateXSRFToken(HttpErrorResponse.headers.get('X-CSRF-TOKEN') || '');
      }
    });
    this.subscriptions.push(sub);
  }

  updateProfilRequest(userId: number){
    if (this.profileForm.valid) {
      const currentEmail = this.userData.userProfil.email;
      const formEmail = this.profileForm.get('email')?.value;

      // Data preparation
        let updateProfilData: RegistrationData = {
        firstName: this.profileForm.value.firstName,
        lastName: this.profileForm.value.lastName,
        email: this.profileForm.value.email,
        password: this.profileForm.value.password,
        secretCode: 'none',
        role: 'user',
        typeProcess: 'updateProfil',
      };

      if (formEmail !== currentEmail) {
        updateProfilData.secretCode = this.secretCodeForm.value.secretCode;
        updateProfilData.typeProcess = 'changeEmail';
      }

      console.log("updateProfilData push in HTTP body query:", updateProfilData);
      const sub = this.customerRequestService.updateProfilForUser(userId, updateProfilData).pipe(take(1)).subscribe({
        next: (response: HttpResponse<any>) => {
          console.log("Request approved: <-------", response);
          this.errorMessage = "";
          this.successMessage = response.body || "Profil mis à jour avec succès.";
          this.userDataStorageService.updateUserProfil({
            ...this.profileForm.value,
            role: this.userData.userProfil.role
          });
          this.startCountdown();
          this.userDataStorageService.updateXSRFToken(response.headers.get('X-CSRF-TOKEN') || '');
        },
        error: (HttpErrorResponse) => {
          console.log("Request rejected: <-------", HttpErrorResponse);
          this.errorMessage = HttpErrorResponse.message;
          this.userDataStorageService.updateXSRFToken(HttpErrorResponse.headers.get('X-CSRF-TOKEN') || '');
        }
      });
      this.subscriptions.push(sub);
    }
  }

  deleteUserRequest(userId: number){
    const sub = this.customerRequestService.deleteUser(userId).pipe(take(1)).subscribe({
      next: (response) => {
        console.log("Request approved: <-------", response);
        this.errorMessage = "";
        this.successMessage = response.body || "Profil supprimé avec succès.";
        this.startCountdown();

        // Deleting user profil, security and save csrf
        this.userDataStorageService.clearUserProfil();
        this.userDataStorageService.clearUserSecurity();
        this.userDataStorageService.updateXSRFToken(response.headers.get('X-CSRF-TOKEN') || '');

        // Transfer to the site home page
        this.router.navigate(['welcome']);
      },
      error: (HttpErrorResponse) => {
        console.log("Request rejected: <-------", HttpErrorResponse);
        this.errorMessage = HttpErrorResponse.message;
        this.userDataStorageService.updateXSRFToken(HttpErrorResponse.headers.get('X-CSRF-TOKEN') || '');
      }
    });
    this.subscriptions.push(sub);
  }

  //////////////////////////////////PASSWORD COMPARISON FUNCTION///////////////////////////
  matchValues( matchTo: string ): (AbstractControl:any) => ValidationErrors | null {
    return (control: AbstractControl): ValidationErrors | null => {
      const parentControls = control.parent?.controls as { [key: string]: AbstractControl<any> };
      return !!control.parent && !!control.parent.value && control.value === parentControls[matchTo].value ? null : { isMatching: false };
    };
  }

  //////////////////////////////////HIDE AND DISPLAY PASSWORD//////////////////////////////
  togglePasswordVisibility() {
    this.hide = !this.hide;
  }

  //////////////////////////////////COUNTDOWN SAVE BUTTON//////////////////////////////////
  startCountdown() {
    const timer$ = interval(1000).pipe(take(this.countdown));
    const subscription = timer$.subscribe(() => {
      this.countdown -= 1;
      if (this.countdown === 0) {
        this.formClosed.emit(true);
        this.dialogRef.close();
      }
    });
    this.subscriptions.push(subscription);
  }

}
