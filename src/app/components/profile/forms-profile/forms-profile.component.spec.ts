import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormsProfileComponent } from './forms-profile.component';

describe('FormsProfileComponent', () => {
  let component: FormsProfileComponent;
  let fixture: ComponentFixture<FormsProfileComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FormsProfileComponent]
    });
    fixture = TestBed.createComponent(FormsProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
