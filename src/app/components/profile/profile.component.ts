import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UserDataStorageService } from 'src/app/services/user-storage/user-data-storage.service';
import { FormsProfileComponent } from './forms-profile/forms-profile.component';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})

export class ProfileComponent {
  //Declaration of variables
  isAuthorized: boolean | undefined;
  dataProfil: any [] = [];
  displayedColProfil: string[] = [];
  panelOneOpenState = false;
  hide: boolean = true;
  userData: any;

  constructor( private userDataStorageService: UserDataStorageService,
               public dialog: MatDialog){

  };

  ngOnInit() {
    //Display user information
    this.userData = this.userDataStorageService.getUserData();

    if (this.userData && this.userData.userSecurity.isAuthorized) {
      //Population du datasource
      this.displayedColProfil = ['property', 'data'];
      this.dataProfil = [
        { property: 'Nom', data: this.userData.userProfil.lastName },
        { property: 'Prénom', data: this.userData.userProfil.firstName },
        { property: 'Email', data: this.userData.userProfil.email },
        { property: 'Mot de passe', data: this.userData.userProfil.password },
        { property: 'Compte', data: this.userData.userProfil.role },
      ];
    } else {
      console.error("User is not authorized or user data not found!");
    }
  }

  openFormProfil(action: string ) {
    const dialogRef = this.dialog.open(FormsProfileComponent, {
      data : {
        action: action,
      }
    });

    if( action === 'update') {
      dialogRef.componentInstance.formClosed.subscribe(() => {
        // Met à jour le profil une fois que le formulaire est fermé
        this.userData = this.userDataStorageService.getUserData();
        if (this.userData && this.userData.userSecurity.isAuthorized) {
          this.dataProfil = [
            { property: 'Nom', data: this.userData.userProfil.lastName },
            { property: 'Prénom', data: this.userData.userProfil.firstName },
            { property: 'Email', data: this.userData.userProfil.email },
            { property: 'Mot de passe', data: this.userData.userProfil.password },
            { property: 'Compte', data: this.userData.userProfil.role },
          ];
        }
      });
    }

  }

  updateProfil(){
    this.openFormProfil('update');
  }

  deleteProfil(){
    this.openFormProfil('delete');
  }

  //////////////////////////////////HIDE AND DISPLAY PASSWORD//////////////////////////////
  togglePasswordVisibility() {
    this.hide = !this.hide;
  }
}
