import { Component, ViewChild } from '@angular/core';
import { BankAccount } from 'src/app/models/bankAccount';
import { FinancialRequestService } from 'src/app/services/financial-request/financial-request.service';
import { FormsBankAccountComponent } from './forms-bank-account/forms-bank-account.component';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { UserDataStorageService } from 'src/app/services/user-storage/user-data-storage.service';

@Component({
  selector: 'app-bank-account',
  templateUrl: './bank-account.component.html',
  styleUrls: ['./bank-account.component.scss']
})
export class BankAccountComponent {

  //Déclaration Variables
  bankAccount = new BankAccount();
  listBankAccount = new MatTableDataSource<BankAccount>([]);
  displayedColBankAccount: string[] = [];
  panelTwoOpenState = false;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  private subscriptions: Subscription[] = [];
  userId: number = 0;

  constructor(private financialRequestService: FinancialRequestService,
              public dialog: MatDialog,
              private userDataStorageService: UserDataStorageService){};

  ngOnInit() {
    // Subscription to Observable to obtain user data
    const userData = this.userDataStorageService.getUserData();
    if (userData && userData.userSecurity.isAuthorized) {
      this.userId = userData.userProfil.id;
    } else {
      console.error("User is not authorized or user data not found!");
    }

    //Colonnes th propriétés bankaccount
    this.displayedColBankAccount = ['referenceAccount', 'typeAccount', 'initialBalance', 'bankName', 'address', 'city', 'Modifier', 'Supprimer'];
    this.loadBankAccountsByUserId(this.userId);
  }

  loadBankAccountsByUserId(id: number){
    const sub = this.financialRequestService.getBankAccountsByUserId(id).pipe(take(1)).subscribe({
      next: (response: HttpResponse<any>) => {
        console.log("Request approved: <-------", response);
        const bankAccounts: BankAccount[] = <BankAccount[]>response.body;
        this.listBankAccount.data = bankAccounts;
        this.listBankAccount.paginator = this.paginator;
        this.listBankAccount.sort = this.sort;
        console.log("Objet received - bankAccounts:", this.listBankAccount);
        this.userDataStorageService.updateXSRFToken(response.headers.get('X-CSRF-TOKEN') || '');
      }
      ,
      error: (error: HttpErrorResponse) => {
        console.log("Request rejected: <-------", error);
        console.error("Error retrieving bankAccount list", error);
        this.userDataStorageService.updateXSRFToken(error.headers.get('X-CSRF-TOKEN') || '');
      }
    });
    this.subscriptions.push(sub);
  }

  openFormBankAccount(action: string, bankAccount?: BankAccount): void {
    const dialogRef = this.dialog.open(FormsBankAccountComponent, {
      data : {
        action: action,
        userId: this.userId,
        bankAccount: bankAccount
      }
    });

    const sub = dialogRef.componentInstance.formClosed.subscribe((result: boolean) => {
      //Reload ListBankAccount for the table
      if (result) {
        this.loadBankAccountsByUserId(this.userId);
      }
    });
    this.subscriptions.push(sub);
  }

  addBankAccount(){
    this.openFormBankAccount('create');
  }

  updateBankAccount(bankAccount: BankAccount){
    this.openFormBankAccount('update', bankAccount);
    console.log("bankaccount selected", bankAccount)
  }

  deleteBankAccount(bankAccount: BankAccount){
    this.openFormBankAccount('delete', bankAccount);
    console.log("bankaccount selected", bankAccount)
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

}

// # Ce programme est un logiciel libre : vous pouvez le redistribuer et/ou le modifier
// # selon les termes de la Licence Publique Générale GNU publiée par la
// # Free Software Foundation, version 3.

// # Ce programme est distribué dans l'espoir qu'il sera utile,
// # mais SANS AUCUNE GARANTIE ; sans même la garantie implicite de
// # COMMERCIALISATION ou d'ADÉQUATION À UN OBJECTIF PARTICULIER. Voir la
// # Licence Publique Générale GNU pour plus de détails.

// # Vous devriez avoir reçu une copie de la Licence Publique Générale GNU
// # avec ce programme. Si ce n'est pas le cas, voir https://www.gnu.org/licenses/.

// # Conditions supplémentaires :
// # 1. Le logiciel peut être utilisé et modifié uniquement dans un cadre personnel ou éducatif (enseignement, apprentissage).
// # 2. Toute redistribution du logiciel dans un but professionnel est interdite.
// # 3. La commercialisation du logiciel est interdite sans l'accord explicite de l'auteur.
// # 4. Toute version modifiée du logiciel utilisée dans un cadre commercial doit rendre le code source disponible.
