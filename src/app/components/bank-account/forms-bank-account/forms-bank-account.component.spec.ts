import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormsBankAccountComponent } from './forms-bank-account.component';

describe('FormsBankAccountComponent', () => {
  let component: FormsBankAccountComponent;
  let fixture: ComponentFixture<FormsBankAccountComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FormsBankAccountComponent]
    });
    fixture = TestBed.createComponent(FormsBankAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
