import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, Inject, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TypeAccount, BankAccount } from 'src/app/models/bankAccount';
import { FinancialRequestService } from 'src/app/services/financial-request/financial-request.service';
import { interval, Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { UserDataStorageService } from 'src/app/services/user-storage/user-data-storage.service';

@Component({
  selector: 'app-forms-bank-account',
  templateUrl: './forms-bank-account.component.html',
  styleUrls: ['./forms-bank-account.component.scss']
})
export class FormsBankAccountComponent {

  //Déclaration Variable
  typeAccounts = Object.values(TypeAccount);
  bankAccountForm: FormGroup;
  success_message = false;
  delete_message = false;
  error = false;
  errormessage = '';
  countdown: number = environment.countdown;
  @Output() formClosed = new EventEmitter<boolean>();
  action: string;
  userId: number;
  bankAccount: BankAccount;
  private subscriptions: Subscription[] = [];

  constructor( public dialogRef: MatDialogRef<FormsBankAccountComponent>,
               @Inject(MAT_DIALOG_DATA) public data: any,
               private formBuilder: FormBuilder,
               private financialRequestService: FinancialRequestService,
               private userDataStorageService: UserDataStorageService) {

    //Récupération Variables du composant Primaire
    this.action = data.action;
    this.userId = data.id;
    this.bankAccount = data.bankAccount || new BankAccount();

    //Initialisation du formulaire
    this.bankAccountForm = this.formBuilder.group({
        id: 0,
        referenceAccount: '',
        bankName: '',
        address: '',
        city: '',
        typeAccount: '',
        initialBalance: [],
        customerId: this.data.userId
    });
  }

  ngOnInit(): void {
    //Vérification champs formulaire
    this.bankAccountForm = this.formBuilder.group({
      id: new FormControl( 0, [
        Validators.required,
      ]),
      referenceAccount: new FormControl('', [
        Validators.required,
        Validators.pattern(/^\d{11}$/),
        Validators.minLength(11),
        Validators.maxLength(11)
      ]),
      bankName: new FormControl('', [
        Validators.required,
        Validators.pattern(/^[A-Za-z,-éèîêùïàçôâ'\s]*$/),
        Validators.minLength(2),
        Validators.maxLength(20),
      ]),
      address: new FormControl('', [
        Validators.required,
        Validators.pattern(/^[A-Za-z0-9°,-:#&éèîêùïàçôâ,'\s]*$/),
        Validators.minLength(2),
        Validators.maxLength(50)
      ]),
      city: new FormControl('', [
        Validators.required,
        Validators.pattern(/^[A-Za-z,-éèîêùïàçôâ'\s]*$/),
        Validators.minLength(2),
        Validators.maxLength(30)
      ]),
      typeAccount: new FormControl('', [
        Validators.required
      ]),
      initialBalance: new FormControl('', [
        Validators.required,
        Validators.pattern(/^-?\d+(\.\d{1,2})?$/)
      ]),
      customerId: new FormControl(this.data.userId, [
        Validators.required,
      ]),
    });

    //Initialisation form value
    if(this.action === 'update' || this.action === 'delete') {
      const typeAccountLabel = BankAccount.prototype.getTypeAccountLabel.call(this.bankAccount, this.data.bankAccount.typeAccount);
      this.bankAccountForm.patchValue({
        referenceAccount: this.data.bankAccount.referenceAccount,
        bankName: this.data.bankAccount.bankName,
        address: this.data.bankAccount.address,
        city: this.data.bankAccount.city,
        typeAccount: typeAccountLabel,
        initialBalance: this.data.bankAccount.initialBalance,
        customerId: this.data.userId
      });
      console.log("typeaccount", this.bankAccountForm.value.typeAccount);
    };
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  sendFormBankAccount(form: FormGroup, action: string){
    console.log("bankAccountForm.valid", this.bankAccountForm.valid);
    if (this.bankAccountForm.valid) {
      //Adaptation typeAccount label => key
      const typeAccountKey = BankAccount.getTypeAccountKey(form.value.typeAccount);
      form.patchValue({ typeAccount: typeAccountKey });

      //Ajustement ID
      if (action === 'create') {
        form.patchValue({ id: 0 });
      } else if (action === 'update') {
        form.patchValue({ id: this.data.bankAccount.id });
      }

      //Observables
      if (action === 'create'){
        this.financialRequestService.addBankAccount(form.value).pipe(take(1)).subscribe({
          next: (HttpResponse) => {
            console.log("Request approved: <-------", HttpResponse);
            this.success_message = true;
            this.userDataStorageService.updateXSRFToken(HttpResponse.headers.get('X-CSRF-TOKEN') || '');
            this.startCountdown();
          },
          error: (HttpErrorResponse) => {
            console.log("Request rejected: <-------", HttpErrorResponse);
            this.error = true;
            this.errormessage = HttpErrorResponse.message;
            this.userDataStorageService.updateXSRFToken(HttpErrorResponse.headers.get('X-CSRF-TOKEN') || '');
          }
        });
      } else if (action === 'update'){
          this.financialRequestService.updateBankAccount(form.value).pipe(take(1)).subscribe({
          next: (HttpResponse) => {
            console.log("Request approved: <-------", HttpResponse);
            this.success_message = true;
            this.userDataStorageService.updateXSRFToken(HttpResponse.headers.get('X-CSRF-TOKEN') || '');
            this.startCountdown();
          },
          error: (HttpErrorResponse) => {
            console.log("Request rejected: <-------", HttpErrorResponse);
            this.error = true;
            this.errormessage = HttpErrorResponse.message;
            this.userDataStorageService.updateXSRFToken(HttpErrorResponse.headers.get('X-CSRF-TOKEN') || '');
          }
        });
      } else if (action === 'delete'){
        console.log("demande de suppression --> requete http")
        this.financialRequestService.deleteBankAccount(this.data.bankAccount.id).pipe(take(1)).subscribe({
          next: (HttpResponse) => {
            console.log("Request approved: <-------", HttpResponse);
            this.delete_message = true;
            this.userDataStorageService.updateXSRFToken(HttpResponse.headers.get('X-CSRF-TOKEN') || '');
            this.startCountdown();
          },
          error: (HttpErrorResponse) => {
            console.log("Request rejected: <-------", HttpErrorResponse);
            this.error = true;
            this.errormessage = HttpErrorResponse.message;
            this.userDataStorageService.updateXSRFToken(HttpErrorResponse.headers.get('X-CSRF-TOKEN') || '');
          }
        });
      }
    } else {
      console.log("bankAccountForm is invalid", form)
    }
  }

  closeFormAddBankAccount(){
    this.dialogRef.close();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  ///////////////////////////////////Methodes Secondaire//////////////////////////////////////

  startCountdown() {
    const timer$ = interval(1000).pipe(take(this.countdown));
    const subscription = timer$.subscribe(() => {
      this.countdown -= 1;
      if (this.countdown === 0) {
        this.formClosed.emit(true);
        this.dialogRef.close();
      }
    });
    this.subscriptions.push(subscription);
  }

}

// # Ce programme est un logiciel libre : vous pouvez le redistribuer et/ou le modifier
// # selon les termes de la Licence Publique Générale GNU publiée par la
// # Free Software Foundation, version 3.

// # Ce programme est distribué dans l'espoir qu'il sera utile,
// # mais SANS AUCUNE GARANTIE ; sans même la garantie implicite de
// # COMMERCIALISATION ou d'ADÉQUATION À UN OBJECTIF PARTICULIER. Voir la
// # Licence Publique Générale GNU pour plus de détails.

// # Vous devriez avoir reçu une copie de la Licence Publique Générale GNU
// # avec ce programme. Si ce n'est pas le cas, voir https://www.gnu.org/licenses/.

// # Conditions supplémentaires :
// # 1. Le logiciel peut être utilisé et modifié uniquement dans un cadre personnel ou éducatif (enseignement, apprentissage).
// # 2. Toute redistribution du logiciel dans un but professionnel est interdite.
// # 3. La commercialisation du logiciel est interdite sans l'accord explicite de l'auteur.
// # 4. Toute version modifiée du logiciel utilisée dans un cadre commercial doit rendre le code source disponible.
