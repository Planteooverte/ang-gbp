import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { FormForgottenPwdComponent } from '../form-forgotten-pwd/form-forgotten-pwd.component';
import { CustomerRequestService } from 'src/app/services/customer-request/customer-request.service';
import { FormRegisterComponent } from '../form-register/form-register.component';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { AuthStorageService } from 'src/app/services/auth-storage/auth-storage.service';
import { interval, Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { UserDataStorageService } from 'src/app/services/user-storage/user-data-storage.service';
import { UserInterface, UserPreference, UserProfil, UserSecurity } from 'src/app/models/userInterface';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-form-login',
  templateUrl: './form-login.component.html',
  styleUrls: ['./form-login.component.scss'],
})

export class FormLoginComponent implements OnInit {
    //Instantiation and variable declaration
    userForm: FormGroup;
    hide = true;
    successMessage: string = '';
    errorMessage: string | null = null;
    countdown: number = environment.countdown;
    @Output() formClosed = new EventEmitter<boolean>();
    private subscriptions: Subscription[] = [];

    //Injecting classes into the constructor
    constructor(
        private formBuilder: FormBuilder,
        private customerRequestService: CustomerRequestService,
        private router: Router,
        private dialog: MatDialog,
        private authStorageService: AuthStorageService,
        private userDataStorageService: UserDataStorageService,
      ) {
      this.userForm = this.formBuilder.group({
        email: '',
        password: '',
      });
    }

    ngOnInit(): void {
      //Checking login and password fields
      this.userForm = this.formBuilder.group({
        email: new FormControl('', [
          Validators.required,
          Validators.email,
          Validators.minLength(6),
          Validators.maxLength(50),
        ]),
        password: new FormControl('', [
          Validators.required,
          Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*\W).*$/),
          Validators.minLength(8),
          Validators.maxLength(50),
        ]),
      });

      //Demonstration user
      this.userForm.setValue({
        email: "test.user.projet.gbp@gmail.com",
        password: "Admin123!"
      });
    }

  ////////////////////////////////METHOD TO REQUEST USER PROFIL////////////////////////////
    profilRequest(userForm: FormGroup) {
      // Data preparation for request.interceptor
      const email = this.userForm.value.email;
      const password = this.userForm.value.password

      // Store authentication information
      this.authStorageService.setCredentials(email, password);

      // Customer Request Service
      const sub = this.customerRequestService.getCustomer({ email, password }).subscribe({
        next: (response: HttpResponse<any>) => {
          console.log("Request approved: <-------", response);

          // Retrieving the customerDTO object and saving it in the UserDataStorageService
          const customerDTO = response.body;

          if (customerDTO) {
            const userInterface = new UserInterface(
              new UserPreference('Défaut',
                                 'Défaut',
                                 'Défaut'),
              new UserSecurity( true,
                                response.headers.get('Authorization') || '',
                                response.headers.get('X-CSRF-TOKEN') || ''),
              new UserProfil(customerDTO.id,
                             customerDTO.firstName,
                             customerDTO.lastName,
                             customerDTO.email,
                             password,
                             customerDTO.role)
            );

            // Stocker les données utilisateur via le nouveau service
            this.userDataStorageService.setUserData(userInterface);

            this.successMessage = 'Bienvenue sur GBP';
            this.startCountdown();
          }


        },
        error: (HttpErrorResponse) => {
          console.log("Request rejected: <-------", HttpErrorResponse);
          this.errorMessage = 'Accès non autorisé. Veuillez vérifier vos informations de connexion';
          this.userDataStorageService.updateXSRFToken(HttpErrorResponse.headers.get('X-CSRF-TOKEN') || '');
        }
      });
      this.subscriptions.push(sub);
    }

    closeFormLogin() {
      this.dialog.closeAll();
    }

    openPwdForgot() {
      this.dialog.closeAll();
      this.dialog.open(FormForgottenPwdComponent);
    }

    openRegisterNewUser() {
      this.dialog.closeAll();
      this.dialog.open(FormRegisterComponent);
    }

    //////////////////////////////////COUNTDOWN SAVE BUTTON//////////////////////////////////
    startCountdown() {
      const timer$ = interval(1000).pipe(take(this.countdown));
      const subscription = timer$.subscribe(() => {
        this.countdown -= 1;
        if (this.countdown === 0) {
          this.formClosed.emit(true);

          // Routing the user
          this.router.navigate(['bank_statement']);

          // close pop-up
          this.dialog.closeAll();
        }
      });
      this.subscriptions.push(subscription);
    }

  }

// # Ce programme est un logiciel libre : vous pouvez le redistribuer et/ou le modifier
// # selon les termes de la Licence Publique Générale GNU publiée par la
// # Free Software Foundation, version 3.

// # Ce programme est distribué dans l'espoir qu'il sera utile,
// # mais SANS AUCUNE GARANTIE ; sans même la garantie implicite de
// # COMMERCIALISATION ou d'ADÉQUATION À UN OBJECTIF PARTICULIER. Voir la
// # Licence Publique Générale GNU pour plus de détails.

// # Vous devriez avoir reçu une copie de la Licence Publique Générale GNU
// # avec ce programme. Si ce n'est pas le cas, voir https://www.gnu.org/licenses/.

// # Conditions supplémentaires :
// # 1. Le logiciel peut être utilisé et modifié uniquement dans un cadre personnel ou éducatif (enseignement, apprentissage).
// # 2. Toute redistribution du logiciel dans un but professionnel est interdite.
// # 3. La commercialisation du logiciel est interdite sans l'accord explicite de l'auteur.
// # 4. Toute version modifiée du logiciel utilisée dans un cadre commercial doit rendre le code source disponible.

