import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicAreaChartComponent } from './basic-area-chart.component';

describe('BasicAreaChartComponent', () => {
  let component: BasicAreaChartComponent;
  let fixture: ComponentFixture<BasicAreaChartComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BasicAreaChartComponent]
    });
    fixture = TestBed.createComponent(BasicAreaChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
