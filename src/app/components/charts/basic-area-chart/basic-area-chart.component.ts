import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-basic-area-chart',
  templateUrl: './basic-area-chart.component.html',
  styleUrls: ['./basic-area-chart.component.scss']
})
export class BasicAreaChartComponent implements OnChanges{
  @Input() dataBalance: any[] = [];

  Highcharts: typeof Highcharts = Highcharts;
  chartOptions: Highcharts.Options = {
    chart: {
        type: 'area',
        width: 400,
        height: 300,
        marginTop: 70,
      },
      title: {
        text: 'Solde Mensuel',
        verticalAlign: 'top',
        y:30,
      },
      xAxis: {
        categories: ['Jan', 'Fév', 'Mar', 'Avr', 'Mai', 'Jui', 'Jul', 'Aoû', 'Sep', 'Oct', 'Nov', 'Déc']
      },
      yAxis: {
        title: {
          text: 'Valeur'
        }
      },
      series: [{
        type: 'area',
        name: 'Total',
        data: this.dataBalance,
        color: 'grey',
        fillColor: 'rgba(200, 200, 200, 0.9)'
      }],
      tooltip: {
        valueSuffix: '€',
        style: {
            fontSize: '18px',
        }
      },
      exporting: {
        enabled: true
      }
  };

  constructor() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['dataBalance'] && changes['dataBalance'].currentValue) {
      this.updateChartWithData(changes['dataBalance'].currentValue,'chart-basic-area', this.chartOptions);
    }
  }

  updateChartWithData(newData: any[], chartId: string, chartOptions: Highcharts.Options): void {
    const chart = this.Highcharts.chart(chartId, chartOptions);
    chart.series[0].setData(newData);
  }
}
