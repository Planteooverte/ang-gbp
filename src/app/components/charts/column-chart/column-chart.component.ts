import { Component, Input, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-column-chart',
  templateUrl: './column-chart.component.html',
  styleUrls: ['./column-chart.component.scss']
})
export class ColumnChartComponent implements OnInit{
  @Input()
  data: any[] = [];

  Highcharts: typeof Highcharts = Highcharts;
  chartOptions: Highcharts.Options = {};

  ngOnInit() {
  this.chartOptions = {
      chart: {
        type: 'column'
      },
      title: {
        text: 'Column Chart with Negative Values'
      },
      xAxis: {
        categories: ['Debit', 'Credit']
      },
      yAxis: {
        title: {
          text: 'Amount'
        }
      },
      series: [{
          name: 'Solde',
          type: 'column',
          data: this.data,
          },
      ],
    };
  }

}
