import { Component, OnChanges, Input, SimpleChanges } from '@angular/core';
import * as Highcharts from 'highcharts';
import { SeriesOptionsType  } from 'highcharts';

@Component({
  selector: 'app-semi-circle-chart-debit',
  templateUrl: './semi-circle-chart-debit.component.html',
  styleUrls: ['./semi-circle-chart-debit.component.scss'],
})

export class SemiCircleChartDebitComponent implements OnChanges{
  @Input() dataDebit: any[] = [];

  Highcharts: typeof Highcharts = Highcharts;
  chartOptions: Highcharts.Options = {
      chart: {
        width: 400,
        height: 300,
        spacing: [0, 0, 0, 0],
        type: 'pie',
        plotBackgroundColor: '#ffffff',
        plotBorderWidth: 0,
        plotShadow: false,
      },
      title: {
        text: 'Semi-Circle Donut Chart',
        align: 'center',
        verticalAlign: 'top',
        y: 40,
      },
      tooltip: {
        pointFormat: '<b>{point.percentage:.1f}%</b>',
        style: {
            fontSize: '18px',
        }
      },
      plotOptions: {
        pie: {
          dataLabels: {
            enabled: false,
          },
          startAngle: -90,
          endAngle: 90,
          size: '140%',
          center: ['50%', '90%'],
        },
      },
      series: [
        {
          type: 'pie',
          name: 'Categorie',
          innerSize: '50%',
          data: this.dataDebit,
        },
      ] as SeriesOptionsType[],
    };

  constructor() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['dataDebit'] && changes['dataDebit'].currentValue) {
      this.updateChartWithData(changes['dataDebit'].currentValue,'chart-debit', this.chartOptions);
    }
  }

  updateChartWithData(newData: any[], chartId: string, chartOptions: Highcharts.Options): void {
    const chart = this.Highcharts.chart(chartId, chartOptions);
    chart.series[0].setData(newData);
  }


  updateChartWithOptions(newOptions: Highcharts.Options): void {
    // Utilisez la méthode update() de Highcharts pour mettre à jour les options du graphique
    const chart = this.Highcharts.chart('chart-debit', newOptions);
  }

}
