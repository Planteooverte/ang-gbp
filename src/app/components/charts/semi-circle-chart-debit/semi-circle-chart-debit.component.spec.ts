import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SemiCircleChartDebitComponent } from './semi-circle-chart-debit.component';

describe('SemiCircleChartComponent', () => {
  let component: SemiCircleChartDebitComponent;
  let fixture: ComponentFixture<SemiCircleChartDebitComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SemiCircleChartDebitComponent]
    });
    fixture = TestBed.createComponent(SemiCircleChartDebitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
