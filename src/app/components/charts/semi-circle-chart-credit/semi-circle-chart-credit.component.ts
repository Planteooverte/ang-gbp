import { Component, OnChanges, Input, SimpleChanges } from '@angular/core';
import * as Highcharts from 'highcharts';
import { SeriesOptionsType  } from 'highcharts';

@Component({
  selector: 'app-semi-circle-chart-credit',
  templateUrl: './semi-circle-chart-credit.component.html',
  styleUrls: ['./semi-circle-chart-credit.component.scss']
})
export class SemiCircleChartCreditComponent implements OnChanges{
  @Input() dataCredit: any[] = [];

  Highcharts: typeof Highcharts = Highcharts;
  chartOptions: Highcharts.Options = {
      chart: {
        width: 400,
        height: 300,
        spacing: [0, 0, 0, 0],
        type: 'pie',
        plotBackgroundColor: '#ffffff',
        plotBorderWidth: 0,
        plotShadow: false,
      },
      title: {
        text: 'Semi-Circle Donut Chart',
        align: 'center',
        verticalAlign: 'top',
        y: 40,
      },
      tooltip: {
        pointFormat: '<b>{point.percentage:.1f}%</b>',
        style: {
            fontSize: '18px',
        }
      },
      plotOptions: {
        pie: {
          dataLabels: {
            enabled: false,
          },
          startAngle: -90,
          endAngle: 90,
          size: '140%',
          center: ['50%', '90%'],
        },
      },
      series: [
        {
          type: 'pie',
          name: 'Categorie',
          innerSize: '50%',
          data: this.dataCredit,
        },
      ] as SeriesOptionsType[],
    };

  constructor() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['dataCredit'] && changes['dataCredit'].currentValue) {
      this.updateChartWithData(changes['dataCredit'].currentValue,'chart-credit', this.chartOptions);
    }
  }

  updateChartWithData(newData: any[], chartId: string, chartOptions: Highcharts.Options): void {
    const chart = this.Highcharts.chart(chartId, chartOptions);
    chart.series[0].setData(newData);
  }


  updateChartWithOptions(newOptions: Highcharts.Options): void {
    // Utilisez la méthode update() de Highcharts pour mettre à jour les options du graphique
    const chart = this.Highcharts.chart('chart-credit', newOptions);
  }

}
