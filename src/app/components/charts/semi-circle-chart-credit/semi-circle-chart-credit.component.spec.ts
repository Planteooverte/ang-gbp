import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SemiCircleChartCreditComponent } from './semi-circle-chart-credit.component';

describe('SemiCircleChartCreditComponent', () => {
  let component: SemiCircleChartCreditComponent;
  let fixture: ComponentFixture<SemiCircleChartCreditComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SemiCircleChartCreditComponent]
    });
    fixture = TestBed.createComponent(SemiCircleChartCreditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
