import { Component, Input, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-semi-circle-chart',
  templateUrl: './semi-circle-chart.component.html',
  styleUrls: ['./semi-circle-chart.component.scss'],
})

export class SemiCircleChartComponent implements OnInit{
  @Input()
  data: any[] = [];

  Highcharts: typeof Highcharts = Highcharts;
  chartOptions: Highcharts.Options = {};

  ngOnInit() {
    this.chartOptions = {
      chart: {
        width: 400,
        height: 300,
        spacing: [0, 0, 0, 0],
        type: 'pie',
        plotBackgroundColor: '#ffffff',
        plotBorderWidth: 0,
        plotShadow: false,
      },
      title: {
        text: 'Semi-Circle Donut Chart',
        align: 'center',
        verticalAlign: 'top',
        y: 40,
      },
      plotOptions: {
        pie: {
          dataLabels: {
            enabled: false,
          },
          startAngle: -90,
          endAngle: 90,
          size: '140%',
          center: ['50%', '90%'],
        },
      },
      series: [
        {
          type: 'pie',
          innerSize: '50%',
          data: this.data,
        },
      ],
    };
  }

}
