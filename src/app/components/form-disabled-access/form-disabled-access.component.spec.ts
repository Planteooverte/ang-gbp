import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormDisabledAccessComponent } from './form-disabled-access.component';

describe('FormDisabledAccessComponent', () => {
  let component: FormDisabledAccessComponent;
  let fixture: ComponentFixture<FormDisabledAccessComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FormDisabledAccessComponent]
    });
    fixture = TestBed.createComponent(FormDisabledAccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
