import { Component, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ContrastDirective } from 'src/app/directives/contrast.directive';
import { userPreferenceInterface } from 'src/app/models/userSession';
import { UserService } from 'src/app/services/user/user.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-form-disabled-access',
  templateUrl: './form-disabled-access.component.html',
  styleUrls: ['./form-disabled-access.component.scss']
})
export class FormDisabledAccessComponent {
  @ViewChild(ContrastDirective) contrastDirective!: ContrastDirective;

  //Déclarations des variables
  contrasts: string[] = [ 'Défaut', 'Renforcé', 'Inversé' ];
  fonts: string[] = [ 'Défaut', 'Adapté' ];
  lineSpacings: string[] = [ 'Défaut', 'Augmenté' ];
  selectedContrast: any = 'Défaut';
  selectedFont: any = 'Défaut';
  selectedLineSpacing: any = 'Défaut';

  userPreferences: userPreferenceInterface = {
    contrast: 'Défaut',
    font: 'Défaut',
    lineSpacing: 'Défaut'
  };

  //Propriété pour stocker l'abonnemenet à l'observable
  private subscription: Subscription | undefined;

  constructor(
    private dialog: MatDialog,
    private userService: UserService,
  ) {}

  ngOnInit() {
    // Souscrire aux préférences utilisateur
      this.subscription=this.userService.userPreferences$.subscribe(preferences => {
      this.userPreferences = preferences;
      console.log("User preference:", preferences)
    });
  }

  ngDoCheck(){
    // Mettre à jour les préférences utilisateur
    const newPreferences: userPreferenceInterface = {
      contrast: this.selectedContrast,
      font: this.selectedFont,
      lineSpacing: this.selectedLineSpacing
    };
    console.log('User preferences selected', newPreferences);

    //Sauvegarde des préférences de l'utilisateur
    this.userService.updateUserPreferences(newPreferences);
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  closeFormDisabledAccess() {
    this.dialog.closeAll();
  }
}

// # Ce programme est un logiciel libre : vous pouvez le redistribuer et/ou le modifier
// # selon les termes de la Licence Publique Générale GNU publiée par la
// # Free Software Foundation, version 3.

// # Ce programme est distribué dans l'espoir qu'il sera utile,
// # mais SANS AUCUNE GARANTIE ; sans même la garantie implicite de
// # COMMERCIALISATION ou d'ADÉQUATION À UN OBJECTIF PARTICULIER. Voir la
// # Licence Publique Générale GNU pour plus de détails.

// # Vous devriez avoir reçu une copie de la Licence Publique Générale GNU
// # avec ce programme. Si ce n'est pas le cas, voir https://www.gnu.org/licenses/.

// # Conditions supplémentaires :
// # 1. Le logiciel peut être utilisé et modifié uniquement dans un cadre personnel ou éducatif (enseignement, apprentissage).
// # 2. Toute redistribution du logiciel dans un but professionnel est interdite.
// # 3. La commercialisation du logiciel est interdite sans l'accord explicite de l'auteur.
// # 4. Toute version modifiée du logiciel utilisée dans un cadre commercial doit rendre le code source disponible.
