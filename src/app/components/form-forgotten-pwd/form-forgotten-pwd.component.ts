import { Component, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { AbstractControl, ValidationErrors } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { CustomerRequestService } from 'src/app/services/customer-request/customer-request.service';
import { FormLoginComponent } from '../form-login/form-login.component';
import { FormRegisterComponent } from '../form-register/form-register.component';
import { ChangePasswordData } from 'src/app/models/changePasswordData';
import { interval, Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { UserDataStorageService } from 'src/app/services/user-storage/user-data-storage.service';

@Component({
  selector: 'app-form-forgotten-pwd',
  templateUrl: './form-forgotten-pwd.component.html',
  styleUrls: ['./form-forgotten-pwd.component.scss']
})
export class FormForgottenPwdComponent {
  //Instantiation and variable declaration
  emailForm: FormGroup;
  passwordSecretCodeForm: FormGroup;
  typeProcess: string = 'forgottenPassword';
  hide: boolean = true;
  errorMessage1: string = '';
  errorMessage2: string = '';
  successMessage: string = '';
  countdown: number = environment.countdown;
  private subscriptions: Subscription[] = [];
  @Output() formClosed = new EventEmitter<boolean>();

  constructor(  private formBuilder: FormBuilder,
                private customerRequestService: CustomerRequestService,
                private userDataStorageService: UserDataStorageService,
                private dialog: MatDialog,
                public dialogRef: MatDialogRef<FormForgottenPwdComponent>){
    this.emailForm = this.formBuilder.group({
      email: '',
    })

    this.passwordSecretCodeForm = this.formBuilder.group({
      secretCode: '',
      password:'',
      passwordControl:'',
    })
  };

  ngOnInit(): void {
    this.emailForm = this.formBuilder.group({
      email: new FormControl('', [
        Validators.required,
        Validators.email,
        Validators.minLength(6),
        Validators.maxLength(50),
      ]),
    })

    this.passwordSecretCodeForm = this.formBuilder.group({
      secretCode: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(6),
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*\W).*$/),
        Validators.minLength(8),
        Validators.maxLength(50),
      ]),
      passwordControl: new FormControl('', [
        Validators.required,
        this.matchValues('password'),
      ]),
    });

    //Demonstration user
    this.emailForm.setValue({
      email: "kevin.massolin.173@gmail.com",
    })
  }

  secretCodeRequest(stepper: any, email: string) {
    if (this.emailForm.valid) {
      const sub = this.customerRequestService.getSecretCode(email, this.typeProcess).pipe(take(1)).subscribe({
        next: (HttpResponse) => {
          console.log("Request approved: <-------", HttpResponse);
          stepper.next();
          this.errorMessage1 = '';
          this.userDataStorageService.updateXSRFToken(HttpResponse.headers.get('X-CSRF-TOKEN') || '');
        },
        error: (HttpErrorResponse) => {
          console.log("Request rejected: <-------", HttpErrorResponse);
          this.errorMessage1 = HttpErrorResponse.message || 'Erreur lors de l\'envoi du code secret';
          this.userDataStorageService.updateXSRFToken(HttpErrorResponse.headers.get('X-CSRF-TOKEN') || '');
        }
      });
      this.subscriptions.push(sub);
    }
  }

  updatePwdRequest() {
    if (this.passwordSecretCodeForm.valid) {
      // Data preparation
      const changePasswordData: ChangePasswordData = {
        email: this.emailForm.value.email,
        password : this.passwordSecretCodeForm.value.password,
        secretCode : this.passwordSecretCodeForm.value.secretCode,
        typeProcess: this.typeProcess
      };

      const sub = this.customerRequestService.updatePwdWithSecretCode(changePasswordData).pipe(take(1)).subscribe({
        next: (HttpResponse) => {
          console.log("Request approved: <-------", HttpResponse);
          this.errorMessage1 = '';
          this.errorMessage2 = '';
          this.successMessage = HttpResponse.body as string;;
          this.startCountdown();
          this.userDataStorageService.updateXSRFToken(HttpResponse.headers.get('X-CSRF-TOKEN') || '');
        },
        error: (HttpErrorResponse) => {
          console.log("Request rejected: <-------", HttpErrorResponse);
          this.errorMessage2 = HttpErrorResponse.message;
          this.userDataStorageService.updateXSRFToken(HttpErrorResponse.headers.get('X-CSRF-TOKEN') || '');
        }
      });
      this.subscriptions.push(sub);
    }
  }

  closeFormForgottenPwd(){
    this.dialog.closeAll();
  }

  openRegisterNewUser(){
    this.dialog.closeAll();
    this.dialog.open(FormRegisterComponent);
  }

  openConnectUser() {
    this.dialog.closeAll();
    this.dialog.open(FormLoginComponent);
  }

  //////////////////////////////////PASSWORD COMPARISON FUNCTION///////////////////////////
  matchValues( matchTo: string ): (AbstractControl:any) => ValidationErrors | null {
    return (control: AbstractControl): ValidationErrors | null => {
      const parentControls = control.parent?.controls as { [key: string]: AbstractControl<any> };
      return !!control.parent && !!control.parent.value && control.value === parentControls[matchTo].value ? null : { isMatching: false };
    };
  }

  //////////////////////////////////HIDE AND DISPLAY PASSWORD//////////////////////////////
  togglePasswordVisibility() {
    this.hide = !this.hide;
  }

  //////////////////////////////////COUNTDOWN LEAVE POP-UP//////////////////////////////////
  startCountdown() {
    const timer$ = interval(1000).pipe(take(this.countdown));
    const subscription = timer$.subscribe(() => {
      this.countdown -= 1;
      if (this.countdown === 0) {
        this.formClosed.emit(true);
        this.dialogRef.close();
        this.dialog.open(FormLoginComponent);
      }
    });
    this.subscriptions.push(subscription);
  }

}

// # Ce programme est un logiciel libre : vous pouvez le redistribuer et/ou le modifier
// # selon les termes de la Licence Publique Générale GNU publiée par la
// # Free Software Foundation, version 3.

// # Ce programme est distribué dans l'espoir qu'il sera utile,
// # mais SANS AUCUNE GARANTIE ; sans même la garantie implicite de
// # COMMERCIALISATION ou d'ADÉQUATION À UN OBJECTIF PARTICULIER. Voir la
// # Licence Publique Générale GNU pour plus de détails.

// # Vous devriez avoir reçu une copie de la Licence Publique Générale GNU
// # avec ce programme. Si ce n'est pas le cas, voir https://www.gnu.org/licenses/.

// # Conditions supplémentaires :
// # 1. Le logiciel peut être utilisé et modifié uniquement dans un cadre personnel ou éducatif (enseignement, apprentissage).
// # 2. Toute redistribution du logiciel dans un but professionnel est interdite.
// # 3. La commercialisation du logiciel est interdite sans l'accord explicite de l'auteur.
// # 4. Toute version modifiée du logiciel utilisée dans un cadre commercial doit rendre le code source disponible.
