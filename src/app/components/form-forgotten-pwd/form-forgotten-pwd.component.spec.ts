import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormForgottenPwdComponent } from './form-forgotten-pwd.component';

describe('FormForgottenPwdComponent', () => {
  let component: FormForgottenPwdComponent;
  let fixture: ComponentFixture<FormForgottenPwdComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FormForgottenPwdComponent]
    });
    fixture = TestBed.createComponent(FormForgottenPwdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
