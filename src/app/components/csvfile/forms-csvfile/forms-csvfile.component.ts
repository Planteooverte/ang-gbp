import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, Inject, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BankAccount, TypeAccount } from 'src/app/models/bankAccount';
import { LongCsvFile } from 'src/app/models/longCsvFile';
import { FileBankingTransaction } from 'src/app/models/file-banking-transaction';
import { FileService } from 'src/app/services/file/file.service';
import { FinancialRequestService } from 'src/app/services/financial-request/financial-request.service';
import { CustvalidatorService } from 'src/app/services/customised-validator/custvalidator.service';
import { interval, Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { UserDataStorageService } from 'src/app/services/user-storage/user-data-storage.service';



@Component({
  selector: 'app-forms-csvfile',
  templateUrl: './forms-csvfile.component.html',
  styleUrls: ['./forms-csvfile.component.scss']
})
export class FormsCsvfileComponent {
  //Variables declaration
  csvFileForm: FormGroup;
  @Output() formClosed = new EventEmitter<boolean>();
  action: string;
  userId: number;
  listBankAccount: BankAccount[] | undefined;
  longCsvFile: LongCsvFile;
  public file: File | null = null;
  private subscriptions: Subscription[] = [];
  success_message = false;
  countdown: number = environment.countdown;
  error = false;
  errormessage = '';
  delete_message = false;
  MAX_FILE_SIZE = environment.MAX_FILE_SIZE;
  fileDropped = false;

  constructor(  public dialogRef: MatDialogRef<FormsCsvfileComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any,
                private formBuilder: FormBuilder,
                private fileService: FileService,
                private custValidatorService: CustvalidatorService,
                private financialRequestService: FinancialRequestService,
                private userDataStorageService: UserDataStorageService){

    //Recovery of Primary component variables
    this.action = data.action;
    this.userId = data.userId;
    this.longCsvFile = data.longCsvFile || new LongCsvFile();

    //Initialising an Checking values from csvFileForm
    this.csvFileForm = this.formBuilder.group({
      id: new FormControl( 0, [
        Validators.required,
      ]),
      referenceAccount: new FormControl(this.longCsvFile.referenceAccount || '', [
        Validators.required,
      ]),
      bankAccountId: new FormControl(this.longCsvFile.bankAccountId || 0),
      fileName: new FormControl(this.longCsvFile.fileName || 'fictive_file.xlsx', [
        Validators.required,
        Validators.pattern(/^[a-zA-Z0-9_.& ()\-éèçàùëêùöô]+$/),
        Validators.minLength(2),
        Validators.maxLength(20),
        this.custValidatorService.fileNameValidator
      ]),
      fileContent: new FormControl(null, [
        Validators.required,
        this.custValidatorService.fileSizeValidator
      ]),
    });

    // Add conditional validation for bankAccountId based on referenceAccount fictivefilename.xlsx
    this.csvFileForm.get('referenceAccount')?.valueChanges.subscribe(value => {
      const bankAccountIdControl = this.csvFileForm.get('bankAccountId');
      if (value) {
        bankAccountIdControl?.setValidators([Validators.required]);
      } else {
        bankAccountIdControl?.clearValidators();
      }
      bankAccountIdControl?.updateValueAndValidity();
    });
  }

  ngOnInit(){
    //Loading the bankaccount drop-down menu
    this.loadBankAccountByUserId(this.userId);

    //Initialisation form value
    if(this.action === 'update' || this.action === 'delete') {
      if (this.data.longCsvFile) {
        this.csvFileForm.patchValue({
          id: this.data.longCsvFile.id || 0,
          referenceAccount: this.data.longCsvFile.referenceAccount || '',
          bankAccountId: this.data.longCsvFile.bankAccountId || 0,
          fileName: this.data.longCsvFile.fileName || '',
          fileContent: null,
        });
      }
      console.log("Date received in forms-csvfile.component",this.data);
      // Disable the fileContent validation for update action
      this.csvFileForm.get('fileContent')?.clearValidators();
      this.csvFileForm.get('fileContent')?.updateValueAndValidity();
    } else {
      // Disable initial fileContent validation for create action
      this.csvFileForm.get('fileContent')?.disable();
    }
  }

  ///////////////////////////////////////////MAIN METHODS/////////////////////////////////////////
  async sendFormCsvFile(form: FormGroup, action: string) {
    console.log("CsvFileForm", form);
    if (action === 'update') {
      //Update bankAccountId
      const referenceAccount = this.csvFileForm.get('referenceAccount')?.value;
        if (referenceAccount) {
        const bankAccountId = BankAccount.getBankAccountId(referenceAccount, this.listBankAccount || []);
        this.csvFileForm.patchValue({
          bankAccountId: bankAccountId,
        });
      }
    }
    if (form.valid) {
      if (action === 'create' && this.fileDropped) {
        // CsvFile Json formatting sent
        const formValue = form.value;
        const payloadCsvFile = {
          fileName: formValue.fileName,
          bankAccount: {
            id: formValue.bankAccountId
          }
        };

        // // TransactionBanking Json formatting sent - 'create'
        const transactions: FileBankingTransaction[] = formValue.fileContent;

        // Observables
        this.financialRequestService.addCsvFile(payloadCsvFile).pipe(take(1)).subscribe({
          next: (response: any) => {
            console.log("Request approved: <-------", response);
            const createdCsvFileId = response.body.id;  // Get back CsvFileId from API
            this.userDataStorageService.updateXSRFToken(response.headers.get('X-CSRF-TOKEN') || '');

            // Bankingtransaction Json formatting sent
            const payloadBankingTransaction = transactions.map(transaction => ({
              date: transaction.date,
              description: transaction.description,
              credit: transaction.credit,
              debit: transaction.debit,
              bankAccount: {
                id: formValue.bankAccountId
              },
              csvFile: {
                id: createdCsvFileId
              }
            }));

            this.financialRequestService.addBankingTransaction(payloadBankingTransaction).pipe(take(1)).subscribe({
              next: (response: any) => {
                console.log("Request approved: <-------", response);
                this.success_message = true;
                this.userDataStorageService.updateXSRFToken(response.headers.get('X-CSRF-TOKEN') || '');
                this.startCountdown();
              },
              error: (HttpErrorResponse) => {
                console.error("Request rejected: <-------", HttpErrorResponse);
                this.error = true;
                this.errormessage = HttpErrorResponse.message;
                this.userDataStorageService.updateXSRFToken(HttpErrorResponse.headers.get('X-CSRF-TOKEN') || '');
              }
            });
          },
          error: (HttpErrorResponse) => {
            console.error("Request rejected: <-------", HttpErrorResponse);
            this.error = true;
            this.errormessage = HttpErrorResponse.message;
            this.userDataStorageService.updateXSRFToken(HttpErrorResponse.headers.get('X-CSRF-TOKEN') || '');
          }
        });
      } else if (action === 'update') {
        this.error = false;
        const payloadCsvFile = {
          id: form.value.id,
          fileName: form.value.fileName,
          bankAccount: {
            id: form.value.bankAccountId
          }
        };
        console.log("payloadCsvFile", payloadCsvFile);
        this.financialRequestService.updateCsvFile(payloadCsvFile).pipe(take(1)).subscribe({
          next: (response: any) => {
            console.log("Request approved: <-------", response);
            this.success_message = true;
            this.userDataStorageService.updateXSRFToken(response.headers.get('X-CSRF-TOKEN') || '');
            this.startCountdown();
          },
          error: (HttpErrorResponse) => {
            console.error("Request rejected: <-------", HttpErrorResponse);
            this.error = true;
            this.errormessage = HttpErrorResponse.message;
            this.userDataStorageService.updateXSRFToken(HttpErrorResponse.headers.get('X-CSRF-TOKEN') || '');
          }
        });
      } else if (action === 'delete'){
        const csvFileId = form.value.id;
        console.log("Suppression CsvfileId", csvFileId);
        this.financialRequestService.deleteCsvFile(csvFileId).pipe(take(1)).subscribe({
          next: (response: any) => {
            console.log("Request approved: <-------", response);
            this.delete_message = true;
            this.userDataStorageService.updateXSRFToken(response.headers.get('X-CSRF-TOKEN') || '');
            this.startCountdown();
          },
          error: (HttpErrorResponse) => {
            console.error("Request rejected: <-------", HttpErrorResponse);
            this.error = true;
            this.errormessage = HttpErrorResponse.message;
            this.userDataStorageService.updateXSRFToken(HttpErrorResponse.headers.get('X-CSRF-TOKEN') || '');
          }
        });
      }
    } else {
      // Reset fileName to an empty string to trigger the associated error messages
      this.csvFileForm.patchValue({
          fileName: ''
      });
      // Display validation errors if the form is invalid
      this.validateAllFormFields(this.csvFileForm);
      if (!this.fileDropped) {
        this.csvFileForm.get('fileContent')?.setErrors({ required: true });
      }
    }
  }

  ///////////////////////////////////SECONDARY METHODS//////////////////////////////////////
  closeFormCsvFile(){
    this.dialogRef.close();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  //////////////////////////////////METHODS FOR BANKACCOUNT DROP-DOWN MENU///////////////////////////
  loadBankAccountByUserId(id: number){
    const sub = this.financialRequestService.getShortListBankAccountsByUserId(id).pipe(take(1)).subscribe({
      next: (response: HttpResponse<any>) => {
        console.log("Request approved: <-------", response);
        const dtoListBankAccounts: BankAccount[] = <BankAccount[]>response.body;
        this.listBankAccount = dtoListBankAccounts;
        console.log("Objet received - bankAccount DTO", this.listBankAccount);
        this.userDataStorageService.updateXSRFToken(response.headers.get('X-CSRF-TOKEN') || '');
      },
      error: (error: HttpErrorResponse) => {
        console.log("Request rejected: <-------", error);
        console.error("Error retrieving bankAccount DTO", error);
        this.userDataStorageService.updateXSRFToken(error.headers.get('X-CSRF-TOKEN') || '');
      }
    });
    this.subscriptions.push(sub);
  }

  getTypeAccountLabel(key: string): string {
    return TypeAccount[key as keyof typeof TypeAccount] || '';
  }

  //////////////////////////////////METHOD RESET SECTION DRAG&DROP SECTION///////////////////////////
  resetSelectedFile(){
    this.file = null;
    const referenceAccount = this.csvFileForm.get('referenceAccount')?.value;
    this.csvFileForm.reset({
      id: 0,
      referenceAccount: referenceAccount,
      fileName: '',
      bankAccountId: 0,
      fileContent: null
    });
    this.error = false;

    // Explicitly set errors to invalidate the form
    this.csvFileForm.get('fileName')?.setErrors({ required: true });
    this.csvFileForm.get('fileContent')?.setErrors({ required: true });

    // Mark all form fields as untouched and clear errors
    this.csvFileForm.markAsPristine();
    this.csvFileForm.markAsUntouched();
    this.csvFileForm.updateValueAndValidity();

    this.error = false;
    this.errormessage = '';
    this.fileDropped = false;
  }

  //////////////////////////////////METHOD CHECK AND PREPARE CSVFILEFORM////////////////////////////
  fileSelected(event: any) {
    const file = event instanceof File ? event : (event.target ? event.target.files[0] : null);
    console.log("file", file);
    if (file) {
      //Variable declaration
      this.file = file;
      this.fileDropped = true;
      this.csvFileForm.get('fileContent')?.enable();

      // Update csvFileForm - field fileName
      this.csvFileForm.patchValue({
        fileName: file.name,
      });

      // Update csvFileForm  - field bankAccountId
      const referenceAccount = this.csvFileForm.get('referenceAccount')?.value;
      if (referenceAccount) {
        const bankAccountId = BankAccount.getBankAccountId(referenceAccount, this.listBankAccount || []);
        this.csvFileForm.patchValue({
          bankAccountId: bankAccountId,
        });
      }

      // Check the filename pattern
      const fileNamePattern = /^[a-zA-Z0-9_.& ()\-éèçàùëêùöô]+$/;
      if (!fileNamePattern.test(file.name)) {
        this.csvFileForm.get('fileName')?.setErrors({ pattern: true });
        return;
      }

      // Checking file name length
      if (file.name.length > 20) {
        this.csvFileForm.get('fileName')?.setErrors({ maxlength: true });
        return;
      }
      if (file.name.length < 2) {
        this.csvFileForm.get('fileName')?.setErrors({ minlength: true });
        return;
      }

      // Check file extension
      const validExtensions = ['.xlsx', '.csv'];
      const fileExtension = file.name.slice((file.name.lastIndexOf('.') - 1 >>> 0) + 2).toLowerCase();
      console.log("fileExtension",fileExtension);
      if (!validExtensions.includes('.' + fileExtension)) {
        console.log("erreur activé");
        this.csvFileForm.get('fileName')?.setErrors({ invalidFileExtension: true });
        return;
      }

      // Check file size
      if (file instanceof File) {
        if (file.size > environment.MAX_FILE_SIZE) {
          this.csvFileForm.get('fileContent')?.setErrors({ invalidFileSize: true });
        } else if (!this.custValidatorService.fileNameValidator(this.csvFileForm.get('fileName')!)?.['invalidFileExtension']) {
          this.csvFileForm.get('fileContent')?.setErrors(null);

          // Read file content and update fileContent
          this.fileService.readFileContent(file)
          .then(transactions => {
            this.csvFileForm.setValue({
              ...this.csvFileForm.value,
              fileContent: transactions
            });

            // Check form validity after updating fileContent
            this.csvFileForm.updateValueAndValidity();
          })
          .catch(error => {
            console.error("Erreur lors de la lecture du fichier:", error);

            // Set error on fileContent control
            this.csvFileForm.get('fileContent')?.setErrors({ fileContentError: true });
          });

        }
      }
    }
  }

  //////////////////////////////////VALIDATOR AND ERRORS//////////////////////////////////
  validateAllFormFields(formGroup: FormGroup) {
    // Mark all form fields as affected to display validation errors
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  getErrorMessage(controlName: string): string {
    // Return custom error messages for each field on the form
    const control = this.csvFileForm.get(controlName);
    if (control && control.errors) {
      if (control.errors['required']) {
        if (controlName === 'fileName') { //FileName ok
          return 'Nom de fichier requis';
        }
        if (controlName === 'fileContent') { //FileContent ok
          return 'Pas de contenu de fichier';
        }
      }
      if (control.hasError('pattern') && controlName === 'fileName') {
        return 'Le nom du fichier contient des caractères non autorisés.';
      }
      if (control.hasError('minlength')) { //FileName ok
      return 'Le nom du fichier doit avoir au moins 2 caractères.';
      }
      if (control.hasError('maxlength')) { //FileName ok
        return 'Le nom du fichier ne doit pas dépasser 20 caractères.';
      }
      if (control.errors['invalidFileSize'] && controlName === 'fileContent') { //FileContent Size ok
      return `La taille du fichier ne doit pas excéder ${this.MAX_FILE_SIZE / 1024} Ko.`;
      }
      if (control.errors['invalidFileExtension'] && controlName === 'fileName') { //FileName ok
        return 'Le fichier doit être au format .xlsx ou .csv';
      }
      if (control.errors['fileContentError'] && controlName === 'fileContent') { // FileContent ok
        return 'Erreur lors de la lecture du fichier.';
      }
    }
    return '';
  }

  //////////////////////////////////COUNTDOWN SAVE BUTTON//////////////////////////////////
  startCountdown() {
    const timer$ = interval(1000).pipe(take(this.countdown));
    const subscription = timer$.subscribe(() => {
      this.countdown -= 1;
      if (this.countdown === 0) {
        this.formClosed.emit(true);
        this.dialogRef.close();
      }
    });
    this.subscriptions.push(subscription);
  }
}

// # Ce programme est un logiciel libre : vous pouvez le redistribuer et/ou le modifier
// # selon les termes de la Licence Publique Générale GNU publiée par la
// # Free Software Foundation, version 3.

// # Ce programme est distribué dans l'espoir qu'il sera utile,
// # mais SANS AUCUNE GARANTIE ; sans même la garantie implicite de
// # COMMERCIALISATION ou d'ADÉQUATION À UN OBJECTIF PARTICULIER. Voir la
// # Licence Publique Générale GNU pour plus de détails.

// # Vous devriez avoir reçu une copie de la Licence Publique Générale GNU
// # avec ce programme. Si ce n'est pas le cas, voir https://www.gnu.org/licenses/.

// # Conditions supplémentaires :
// # 1. Le logiciel peut être utilisé et modifié uniquement dans un cadre personnel ou éducatif (enseignement, apprentissage).
// # 2. Toute redistribution du logiciel dans un but professionnel est interdite.
// # 3. La commercialisation du logiciel est interdite sans l'accord explicite de l'auteur.
// # 4. Toute version modifiée du logiciel utilisée dans un cadre commercial doit rendre le code source disponible.
