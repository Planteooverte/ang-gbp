import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormsCsvfileComponent } from './forms-csvfile.component';

describe('FormsCsvfileComponent', () => {
  let component: FormsCsvfileComponent;
  let fixture: ComponentFixture<FormsCsvfileComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FormsCsvfileComponent]
    });
    fixture = TestBed.createComponent(FormsCsvfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
