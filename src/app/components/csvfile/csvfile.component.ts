import { Component, ViewChild } from '@angular/core';
import { LongCsvFile } from 'src/app/models/longCsvFile';
import { FinancialRequestService } from 'src/app/services/financial-request/financial-request.service';
import { FormsCsvfileComponent } from './forms-csvfile/forms-csvfile.component';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { UserDataStorageService } from 'src/app/services/user-storage/user-data-storage.service';


@Component({
  selector: 'app-csvfile',
  templateUrl: './csvfile.component.html',
  styleUrls: ['./csvfile.component.scss']
})
export class CsvfileComponent {

  //variable declaration
  panelOneOpenState = false;
  listCvsFile = new MatTableDataSource<LongCsvFile>([]);
  displayedCvsFile: string[] = [];
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  private subscriptions: Subscription[] = [];
  longCsvFile = new LongCsvFile();
  userId: number = 0;

  constructor(  private financialRequestService: FinancialRequestService,
                public dialog: MatDialog,
                private userDataStorageService: UserDataStorageService){};

  ngOnInit(){
    // Subscription to Observable to obtain user data
    const userData = this.userDataStorageService.getUserData();
    if (userData && userData.userSecurity.isAuthorized) {
      this.userId = userData.userProfil.id;
    } else {
      console.error("User is not authorized or user data not found!");
    }

    //Colonnes th propriétés csvfile
    this.displayedCvsFile = ['fileName', 'creationDate', 'referenceAccount', 'bankName', 'typeAccount', 'Modifier', 'Supprimer'];
    this.loadCsvFileByUserId(this.userId);
  }

  /////////////////////////////////////////////MAIN METHOD/////////////////////////////////////////
  loadCsvFileByUserId(id: number) {
    console.log("ID User:", this.userId);
    const sub = this.financialRequestService.getCsvFilesByUserId(id).pipe(take(1)).subscribe({
      next: (response: HttpResponse<any>) => {
        console.log("Request approved: <-------", response);
        const csvFiles: LongCsvFile[] = <LongCsvFile[]>response.body;
        this.listCvsFile.data = csvFiles;
        this.listCvsFile.paginator = this.paginator;
        this.listCvsFile.sort = this.sort;
        console.log("Objet received - csvFiles:", this.listCvsFile);
        this.userDataStorageService.updateXSRFToken(response.headers.get('X-CSRF-TOKEN') || '');
      },
      error: (error: HttpErrorResponse) => {
        console.log("Request rejected: <-------", error);
        console.error("Error retrieving csvfile list", error);
        this.userDataStorageService.updateXSRFToken(error.headers.get('X-CSRF-TOKEN') || '');
      }
    });
    this.subscriptions.push(sub);
  }

  /////////////////////////////////////////////DISPLAY DATE IN HTML/////////////////////////////////////////
  formatDateAndTime(dateTime: string): string {
    const date = new Date(dateTime);
    const formattedDate = `${date.getFullYear()}-${(date.getMonth() + 1).toString().padStart(2, '0')}-${date.getDate().toString().padStart(2, '0')}`;
    const formattedTime = `${date.getHours().toString().padStart(2, '0')}:${date.getMinutes().toString().padStart(2, '0')}`;
    return `${formattedDate} ${formattedTime}`;
  }

  /////////////////////////////////////////////BUTTON METHODS//////////////////////////////////////////////
  openFormCsvFile(action: string, longCsvFile?: LongCsvFile): void {
    const dialogRef = this.dialog.open(FormsCsvfileComponent, {
      data : {
        action: action,
        userId: this.userId,
        longCsvFile: longCsvFile
      }
    });

    const sub = dialogRef.componentInstance.formClosed.subscribe((result: boolean) => {
      //Reload ListCsvFiles for the table
      if (result) {
        this.loadCsvFileByUserId(this.userId);
      }
    });
    this.subscriptions.push(sub);
  }

  addCsvFile(){
    this.openFormCsvFile('create');
  }

  updateCsvFile(longCsvFile: LongCsvFile){
    console.log("Data Transmission Primary To Secondary -longCsvFile", longCsvFile);
    this.openFormCsvFile('update', longCsvFile);
  }

  deleteCsvFile(longCsvFile: LongCsvFile){
    console.log("Data Transmission Primary To Secondary -longCsvFile", longCsvFile);
    this.openFormCsvFile('delete', longCsvFile);
  }

  ////////////////////////////////////MEMORY CLEANING//////////////////////////////////////
  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }
}

// # Ce programme est un logiciel libre : vous pouvez le redistribuer et/ou le modifier
// # selon les termes de la Licence Publique Générale GNU publiée par la
// # Free Software Foundation, version 3.

// # Ce programme est distribué dans l'espoir qu'il sera utile,
// # mais SANS AUCUNE GARANTIE ; sans même la garantie implicite de
// # COMMERCIALISATION ou d'ADÉQUATION À UN OBJECTIF PARTICULIER. Voir la
// # Licence Publique Générale GNU pour plus de détails.

// # Vous devriez avoir reçu une copie de la Licence Publique Générale GNU
// # avec ce programme. Si ce n'est pas le cas, voir https://www.gnu.org/licenses/.

// # Conditions supplémentaires :
// # 1. Le logiciel peut être utilisé et modifié uniquement dans un cadre personnel ou éducatif (enseignement, apprentissage).
// # 2. Toute redistribution du logiciel dans un but professionnel est interdite.
// # 3. La commercialisation du logiciel est interdite sans l'accord explicite de l'auteur.
// # 4. Toute version modifiée du logiciel utilisée dans un cadre commercial doit rendre le code source disponible.
