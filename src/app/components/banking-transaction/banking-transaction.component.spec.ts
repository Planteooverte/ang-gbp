import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BankingTransactionComponent } from './banking-transaction.component';

describe('BankingTransactionComponent', () => {
  let component: BankingTransactionComponent;
  let fixture: ComponentFixture<BankingTransactionComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BankingTransactionComponent]
    });
    fixture = TestBed.createComponent(BankingTransactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
