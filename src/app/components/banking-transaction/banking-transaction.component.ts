import { Component, ViewChild, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { BankingTransaction } from 'src/app/models/bankingTransaction';
import { BankAccount, TypeAccount } from 'src/app/models/bankAccount';
import { Category } from 'src/app/models/category';
import { FinancialRequestService } from 'src/app/services/financial-request/financial-request.service';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription, take } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { FormControl } from '@angular/forms';
import { SelectionModel } from '@angular/cdk/collections';
import { UserDataStorageService } from 'src/app/services/user-storage/user-data-storage.service';

@Component({
  selector: 'app-banking-transaction',
  templateUrl: './banking-transaction.component.html',
  styleUrls: ['./banking-transaction.component.scss']
})
export class BankingTransactionComponent implements AfterViewInit {
  //Déclaration variables
  panelTwoOpenState = false;
  listBankingTransaction = new MatTableDataSource<BankingTransaction>([]);
  listBankAccount: BankAccount[] | undefined;
  listCategory: Category[] | undefined;
  filteredCategories: Category[] = [];
  columnBankingTransaction: string[] = [];
  selectedBankAccountId: number = 0;
  selectedPeriod: string = '';
  selectedTransactionType: string = 'all';
  selectedDescription: FormControl = new FormControl('');
  selectedCheckboxes = new SelectionModel<BankingTransaction>(true, []);
  selectedCategory: string = '';
  uniquePeriods: string[] = [];
  isDisplayed = false;
  isCategoryDropdownVisible = false;
  shouldDisplayCheckboxes = false;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  errormessage: string | undefined;
  error = false;
  private subscriptions: Subscription[] = [];
  userId: number = 0;

  constructor(private financialRequestService: FinancialRequestService,
              private cdr: ChangeDetectorRef,
              private userDataStorageService: UserDataStorageService){}

  ngOnInit(){
    // Subscription to Observable to obtain user data
    const userData = this.userDataStorageService.getUserData();
    if (userData && userData.userSecurity.isAuthorized) {
      this.userId = userData.userProfil.id;
    } else {
      console.error("User is not authorized or user data not found!");
    }

    //Loading the bankaccount drop-down menu
    this.loadBankAccountByUserId(this.userId);

    //Loading the category drop-down menu
    this.loadCategory();

    //Display user banking transaction information
    this.columnBankingTransaction = ['date', 'description', 'credit', 'debit', 'fileName', 'categoryName'];
  }

  ngAfterViewInit(): void {
    this.listBankingTransaction.paginator = this.paginator;
    this.listBankingTransaction.sort = this.sort;
    this.cdr.detectChanges();
  }

  //////////////////////////////////////DROP-DOWN MENU FOR LIST BANKACCOUNT////////////////////////
  loadBankAccountByUserId(id: number){
    const sub = this.financialRequestService.getShortListBankAccountsByUserId(id).pipe(take(1)).subscribe({
      next: (response: HttpResponse<any>) => {
        console.log("Request approved: <-------", response);
        const dtoListBankAccounts: BankAccount[] = <BankAccount[]>response.body;
        this.listBankAccount = dtoListBankAccounts;
        console.log("Objet received - bankAccount DTO", this.listBankAccount);
        this.userDataStorageService.updateXSRFToken(response.headers.get('X-CSRF-TOKEN') || '');
      },
      error: (error: HttpErrorResponse) => {
        console.log("Request rejected: <-------", error);
        this.userDataStorageService.updateXSRFToken(error.headers.get('X-CSRF-TOKEN') || '');
      }
    });
    this.subscriptions.push(sub);
  }

  getTypeAccountLabel(key: string): string {
    return TypeAccount[key as keyof typeof TypeAccount] || '';
  }

  /////////////////////////////////////FILTER BY BANKACCOUNT - BANKING TRANSACTION////////////////
  loadBankingTransactionByBankAccountId(id: number){

    // Display and Reset period to 'all' when bank account changes
    this.isDisplayed = true;
    this.selectedPeriod = 'all';
    this.uniquePeriods = ['all'];

    const sub = this.financialRequestService.getBankingTransactionByBankAccountId(id).pipe(take(1)).subscribe({
      next: (response: HttpResponse<any>) => {
        console.log("Request approved: <-------", response);
        let bankTransactions: BankingTransaction[] = <BankingTransaction[]>response.body;

        // Capitalize the first letter of each category name
        bankTransactions = bankTransactions.map(transaction => ({
          ...transaction,
          categoryName: this.capitalizeFirstLetter(transaction.categoryName)
        }));

        this.listBankingTransaction.data = bankTransactions;
        this.listBankingTransaction.paginator = this.paginator;
        this.listBankingTransaction.sort = this.sort;
        console.log("Objet received - bankingTransaction", this.listBankingTransaction);

        // Reset error section
        this.error = false;

        //Build drop-down period menu
        this.uniquePeriods = ['all', ...new Set(bankTransactions.map(transaction => this.formatPeriod(transaction.date)))];
        this.userDataStorageService.updateXSRFToken(response.headers.get('X-CSRF-TOKEN') || '');

        this.loadBankingTransactionWithFilter();
      },
      error: (error: HttpErrorResponse) => {
        console.log("Request rejected: <-------", error);
        this.error = true;
        if (error.status === 404) {
          this.errormessage = `No banking transactions found`;
        } else {
          this.errormessage = `Error retrieving banking transactions: ${error.message}`;
        }
        this.userDataStorageService.updateXSRFToken(error.headers.get('X-CSRF-TOKEN') || '');
      }
    });
    this.subscriptions.push(sub);
  }

  ///////////////////////////////////CHECKBOX METHODS/////////////////////////////////////////////
  // Selection methods for checkboxes
  isAllSelected() {
    const numSelected = this.selectedCheckboxes.selected.length;
    const numRows = this.listBankingTransaction.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
        this.selectedCheckboxes.clear() :
        this.listBankingTransaction.data.forEach(row => this.selectedCheckboxes.select(row));
  }

  //////////////////////////////////////DROP-DOWN MENU FOR LIST CATEGORY////////////////////////
  loadCategory(){
    const sub = this.financialRequestService.getCategory().pipe(take(1)).subscribe({
      next: (response: HttpResponse<any>) => {
        console.log("Request approved: <-------", response);
        const dtoListCategory: Category[] = <Category[]>response.body;
        this.listCategory = dtoListCategory;
        console.log("Objet received - category DTO", this.listCategory);
        this.userDataStorageService.updateXSRFToken(response.headers.get('X-CSRF-TOKEN') || '');
      },
      error: (error: HttpErrorResponse) => {
        console.log("Request rejected: <-------", error);
        this.userDataStorageService.updateXSRFToken(error.headers.get('X-CSRF-TOKEN') || '');
      }
    });
    this.subscriptions.push(sub);
  }

  filterCategories() {
    if (this.selectedTransactionType !== 'all') {
      this.filteredCategories = this.listCategory?.filter(category => category.categoryType.toLowerCase() === this.selectedTransactionType.toLowerCase()) || [];
    } else {
      this.filteredCategories = this.listCategory || [];
    }
    // Capitalize the first letter of each category name
    this.filteredCategories = this.filteredCategories.map(category => ({
      ...category,
      categoryName: this.capitalizeFirstLetter(category.categoryName)
    }));
  }

  capitalizeFirstLetter(text: string): string {
    return text.charAt(0).toUpperCase() + text.slice(1).toLowerCase();
  }

  checkCategoryDropdownVisibility() {
    this.isCategoryDropdownVisible = this.isDisplayed === true && this.selectedTransactionType !== 'all';
  }

  ///////////////////////////////////FILTER METHOD/////////////////////////////////////////////////
  loadBankingTransactionWithFilter() {
    const periodFilter = this.selectedPeriod;
    const transactionTypeFilter = this.selectedTransactionType.toLowerCase();
    const descriptionFilter = this.selectedDescription.value.trim().toLowerCase();

    this.listBankingTransaction.filterPredicate = (data: BankingTransaction, filter: string) => {
      const matchesPeriod = periodFilter === 'all' || this.formatPeriod(data.date) === periodFilter;
      const matchesType = transactionTypeFilter === 'all' || data.transactionType.toLowerCase() === transactionTypeFilter;
      const matchesDescription = !descriptionFilter || data.description.toLowerCase().includes(descriptionFilter);

      return matchesPeriod && matchesType && matchesDescription;
    };

    // Forcing the MatTableDataSource to re-evaluate the filter
    this.listBankingTransaction.filter = Math.random().toString();

    // Check and display category drop-down on transactionTypeFilter change
    this.checkCategoryDropdownVisibility();
    this.filterCategories();

    // Update checkboxes display
    this.shouldDisplayCheckboxes = transactionTypeFilter === 'debit' || transactionTypeFilter === 'credit';

    // Update the number of column displayed based on shouldDisplayCheckboxes boolean
    if (this.shouldDisplayCheckboxes) {
      this.columnBankingTransaction = ['date', 'description', 'credit', 'debit', 'fileName', 'categoryName', 'select'];
    } else {
      this.columnBankingTransaction = ['date', 'description', 'credit', 'debit', 'fileName', 'categoryName'];
    }
  }

  ///////////////////////////////////FORMAT DATE METHOD////////////////////////////////////////////
  formatPeriod(date: string): string {
    const d = new Date(date);
    return `${d.getMonth() + 1}/${d.getFullYear()}`;
  }

  ///////////////////////////////////CLEAR FILTER////////////////////////////////////////////////
  clearFilter() {
    this.selectedDescription.setValue('');
    this.selectedTransactionType = 'all';
    this.selectedPeriod = 'all';
    this.loadBankingTransactionWithFilter();
  }

  ///////////////////////////////////ADD CATEGORY IN BANKING TRANSACTION//////////////////////////
  addCategory(){
    const payload = this.createUpdatePayload();
    const sub = this.financialRequestService.updateBankingTransactionOnCategoryId(payload).pipe(take(1)).subscribe({
      next: (response: any) => {
        console.log("Request approved: <-------", response);
        this.loadBankingTransactionByBankAccountId(this.selectedBankAccountId);
        this.loadBankingTransactionWithFilter();
        this.selectedCheckboxes.clear();
        this.userDataStorageService.updateXSRFToken(response.headers.get('X-CSRF-TOKEN') || '');
      },
      error: (HttpErrorResponse) => {
        console.log("Request rejected: <-------", HttpErrorResponse);
        this.error = true;
        this.errormessage = HttpErrorResponse.message?.text || `Error updating categories: ${HttpErrorResponse.message}`;
        this.listBankingTransaction.data = [];
        this.userDataStorageService.updateXSRFToken(HttpErrorResponse.headers.get('X-CSRF-TOKEN') || '');
      }
    });
    this.subscriptions.push(sub);
  }

  // Create object payload to update category in Banking Transactions
  createUpdatePayload() {
    const selectedTransactions = this.selectedCheckboxes.selected;
    const normalizedSelectedCategory = this.selectedCategory.toLowerCase();
    const selectedCategoryId = Category.findCategoryIdByName(this.listCategory || [], normalizedSelectedCategory);

    // Filter transactions by selected transaction type
    const transactionTypeFilter = this.selectedTransactionType.toLowerCase();

    return selectedTransactions
    .filter(transaction => transaction.transactionType.toLowerCase() === transactionTypeFilter)
    .map(transaction => ({
      id: transaction.id,
      csvFileId: transaction.csvFileId,
      categoryId: selectedCategoryId,
      bankAccountId: this.selectedBankAccountId
    }));
  }

  ///////////////////////////////////CLEANING OBSERVABLES/////////////////////////////////////////
  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }
}

// # Ce programme est un logiciel libre : vous pouvez le redistribuer et/ou le modifier
// # selon les termes de la Licence Publique Générale GNU publiée par la
// # Free Software Foundation, version 3.

// # Ce programme est distribué dans l'espoir qu'il sera utile,
// # mais SANS AUCUNE GARANTIE ; sans même la garantie implicite de
// # COMMERCIALISATION ou d'ADÉQUATION À UN OBJECTIF PARTICULIER. Voir la
// # Licence Publique Générale GNU pour plus de détails.

// # Vous devriez avoir reçu une copie de la Licence Publique Générale GNU
// # avec ce programme. Si ce n'est pas le cas, voir https://www.gnu.org/licenses/.

// # Conditions supplémentaires :
// # 1. Le logiciel peut être utilisé et modifié uniquement dans un cadre personnel ou éducatif (enseignement, apprentissage).
// # 2. Toute redistribution du logiciel dans un but professionnel est interdite.
// # 3. La commercialisation du logiciel est interdite sans l'accord explicite de l'auteur.
// # 4. Toute version modifiée du logiciel utilisée dans un cadre commercial doit rendre le code source disponible.
