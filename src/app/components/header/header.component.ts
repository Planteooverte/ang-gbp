import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { FormDisabledAccessComponent } from '../form-disabled-access/form-disabled-access.component';
import { FormLoginComponent } from '../form-login/form-login.component';
import { Router } from '@angular/router';
import { UserDataStorageService } from 'src/app/services/user-storage/user-data-storage.service';
import { CustomerRequestService } from 'src/app/services/customer-request/customer-request.service';
import { Subscription } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { take } from 'rxjs/operators';
import { DisconnectComponent } from './disconnect/disconnect.component';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit{

  isAuthorized: boolean | undefined;
  showPersonTooltip: boolean = false;
  private subscriptions: Subscription[] = [];

  constructor ( private dialog: MatDialog,
                private router: Router,
                private customerRequestService: CustomerRequestService,
                private userDataStorageService: UserDataStorageService,){
  }

  ngOnInit() {
    this.userDataStorageService.getIsAuthorized$().subscribe((isAuthorized) => {
      this.isAuthorized = isAuthorized;
    });
  }

  openFormDisabledAccess(){
    console.log('open Form-disabled-access');
    console.log('open Form-disabled-access');
    this.dialog.open(FormDisabledAccessComponent);
  }

  openFormLogin(){
    console.log('open Form-login');
    this.dialog.open(FormLoginComponent);
  }

  logoutRequest(){
    console.log("Disconnexion request:");

    //Data preparation
    const userData = this.userDataStorageService.getUserData();
    const userId = userData?.userProfil.id;
    if (userId === undefined) {
      console.error("User ID is undefined. User data might be null.");
      return;
    }

    const sub = this.customerRequestService.deleteJWTforUser(userId).pipe(take(1)).subscribe({
      next: (response: HttpResponse<any>) => {
        console.log("Request approved: <-------", response);
        this.dialog.open(DisconnectComponent);
        this.userDataStorageService.updateXSRFToken(response.headers.get('X-CSRF-TOKEN') || '');
      },
      error: (HttpErrorResponse) => {
        console.log("Request rejected: <-------", HttpErrorResponse);
        this.userDataStorageService.updateXSRFToken(HttpErrorResponse.headers.get('X-CSRF-TOKEN') || '');
      }
    });
    this.subscriptions.push(sub);

    // Deleting user profil and security
    this.userDataStorageService.clearUserProfil();
    this.userDataStorageService.clearUserSecurity();

    // Transfer to the site home page
    this.router.navigate(['welcome']);
  }
}
