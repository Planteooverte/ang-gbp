import { Component } from '@angular/core';
import { environment } from "../../../../environments/environment";
import { interval, Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-disconnect',
  templateUrl: './disconnect.component.html',
  styleUrls: ['./disconnect.component.scss']
})
export class DisconnectComponent {

  successMessage: string = '';
  countdown: number = environment.countdown;
  private subscriptions: Subscription[] = [];

  constructor( private dialog: MatDialog ){
    this.successMessage = 'Vous êtes bien déconnectés. À bientôt.';
    this.startCountdown();
  }

  //////////////////////////////////COUNTDOWN LEAVE MESSAGE//////////////////////////////////
    startCountdown() {
      const timer$ = interval(1000).pipe(take(this.countdown));
      const subscription = timer$.subscribe(() => {
        this.countdown -= 1;
        if (this.countdown === 0) {
          this.successMessage = '';
          this.dialog.closeAll();
        }
      });
      this.subscriptions.push(subscription);
    }
}
