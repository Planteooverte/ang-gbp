import { Component } from '@angular/core';
import { FormContactComponent } from '../form-contact/form-contact.component';
import { CguComponent } from '../cgu/cgu.component';
import { MatDialog } from '@angular/material/dialog';


@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent {

  constructor(private dialog: MatDialog) {}

  openFormcontact(){
    console.log('open FormContactComponent');
    this.dialog.open(FormContactComponent);
  }

  openCgu(){
    console.log('open CguComponent');
    this.dialog.open(CguComponent);
  }
}
