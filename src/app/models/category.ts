export class Category{
  //Déclaration des propriétés
  public id: number;
  public categoryName: string;
  public categoryType: string;

  //Préparation du constructeur
  constructor(id?: number, categoryName?: string, categoryType?: string){
    this.id = id || 0;
    this.categoryName = categoryName || '';
    this.categoryType = categoryType || '';
  }

  // Méthode pour trouver l'id de la catégorie par son nom
  static findCategoryIdByName(categories: Category[], categoryName: string): number | undefined {
    const category = categories.find(cat => cat.categoryName === categoryName);
    return category ? category.id : undefined;
  }
}
