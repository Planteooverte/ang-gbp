export interface PercentageCatStatDTO {
  year: number;
  month: number;
  categoryName: string;
  totalCatCredit: number | null;
  totalCatDebit: number | null;
  percentTotalCatCredit: number | null;
  percentTotalCatDebit: number | null;
}
