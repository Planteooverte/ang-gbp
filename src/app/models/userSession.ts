export interface userSession {
    userPreferenceInterface : userPreferenceInterface;
}

export interface userPreferenceInterface {
  contrast: string,
  font: string,
  lineSpacing: string,
}
