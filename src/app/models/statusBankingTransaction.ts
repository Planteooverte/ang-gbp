export interface StatusBankingTransaction {
  type: string;
  [key: number]: StatusTransaction | TransactionsOnly;
}

export interface StatusTransaction {
  status: boolean;
  transactions: number | null;
}

export interface TransactionsOnly {
  transactions: number | null;
}
