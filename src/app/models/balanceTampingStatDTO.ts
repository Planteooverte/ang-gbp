export interface BalanceTampingStatDTO {
  year: number;
  month: number;
  totalDebit: number;
  totalCredit: number;
  tampingBalance: number;
}
