export class BankAccount{

  //Déclaration des propriétés
  public id: number;
  public referenceAccount: string;
  public bankName: string;
  public address : string;
  public city : string;
  public typeAccount: string;
  public initialBalance: number;

  //Préparation du constructeur
  constructor(id?: number, referenceAccount?: string, bankName?: string, address?: string,
              city?: string, typeAccount?: TypeAccount, initialBalance?: number){
        this.id = id || 0;
        this.referenceAccount = referenceAccount || '00000000000';
        this.bankName = bankName || '';
        this.address = address || '';
        this.city = city || '';
        this.typeAccount = typeAccount || TypeAccount.Non_defini;
        this.initialBalance = initialBalance || 0;
  }

  // Méthode pour obtenir le label du TypeAccount à partir de la clé
  getTypeAccountLabel(key: string): string {
    return TypeAccount[key as keyof typeof TypeAccount] || '';
  }

  // Méthode pour obtenir la clé du TypeAccount à partir du label
  static getTypeAccountKey(label: string): string {
    return Object.keys(TypeAccount).find(key => TypeAccount[key as keyof typeof TypeAccount] === label) || '';
  }

  //Méthode pour obtenir un bankaccountid à partir d'une liste de bankaccount et d'un referenceAccount
  static getBankAccountId(referenceAccount: string, bankAccounts: BankAccount[]): number | undefined {
    const account = bankAccounts.find(account => account.referenceAccount === referenceAccount);
    return account ? account.id : undefined;
  }
}

export enum TypeAccount {
  Assurance_vie = 'Assurance vie',
  CAT = 'Compte à terme',
  CEL = 'Compte épargne logement',
  Compte_courant = 'Compte courant',
  Compte_joint = 'Compte joint',
  LDDS = 'Livret de développement durable et solidaire',
  Livret_A = 'Livret A',
  LEP = 'Livret d\'épargne populaire',
  PEA = 'Plan d\'épargne en action',
  PEL = 'Plan d\'épargne logement',
  PEE = 'Plan d\'épargne entreprise',
  PERP = 'Plan d\'épargne retraite populaire',
  Non_defini = 'Non défini',
}



