export interface NbTransactionStatDTO {
  year: number;
  month: number;
  attendanceRecord: boolean;
  nbTransactions: number | null;
}
