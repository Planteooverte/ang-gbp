import { Decimal } from 'decimal.js';

export class BankingTransaction{
  //Déclaration des propriétés
  public id: number;
  public date: string;
  public description : string;
  public credit : Decimal;
  public debit: Decimal;
  public categoryId: number;
  public csvFileId: number;
  public fileName: string;
  public categoryName: string;
  public transactionType: string;

  //Préparation du constructeur
  constructor(id?: number, date?: string, description?: string, credit?: Decimal,
              debit?: Decimal, categoryId?: number, csvFileId?: number,
              fileName?: string, categoryName?: string, transactionType?: string){
    this.id = id || 0;
    this.date = date ||  '';
    this.description = description || '';
    this.credit = credit instanceof Decimal ? credit : new Decimal(credit || 0);
    this.debit = debit instanceof Decimal ? debit : new Decimal(debit || 0);
    this.categoryId = categoryId || 0;
    this.csvFileId = csvFileId || 0;
    this.fileName = fileName || '';
    this.categoryName = categoryName || '';
    this.transactionType =  transactionType || '';
  }
}
