export interface FileBankingTransaction {
  date: string;
  description: string;
  credit: number | null;
  debit: number | null;
}
