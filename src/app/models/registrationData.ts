export class RegistrationData{
  //Property declaration
  public firstName: string;
  public lastName: string;
  public email : string;
  public password: string;
  public secretCode : string;
  public role : string;
  public typeProcess : string;

  //Preparation contructor
  constructor(  firstName?: string, lastName?: string, email?: string,
                password?: string, secretCode?: string, role?: string,
                typeProcess?: string){
    this.firstName = firstName || '';
    this.lastName = lastName || '';
    this.email = email || '';
    this.password = password || '';
    this.secretCode = secretCode || '';
    this.role = role || '';
    this.typeProcess = typeProcess || '';
  }
}
