// Class UserInterface
export class UserInterface {
  userPreference: UserPreference;
  userSecurity: UserSecurity;
  userProfil: UserProfil;

  constructor(userPreference: UserPreference, userSecurity: UserSecurity, userProfil: UserProfil) {
    this.userPreference = userPreference;
    this.userSecurity = userSecurity;
    this.userProfil = userProfil;
  }
}

// Class UserPreference
export class UserPreference {
  contrast: string;
  font: string;
  lineSpacing: string;

  constructor(contrast: string, font: string, lineSpacing: string) {
    this.contrast = contrast || 'Defaut';
    this.font = font || 'Defaut';
    this.lineSpacing = lineSpacing || 'Defaut';
  }
}

// Class UserSecurityData
export class UserSecurity {
  isAuthorized: boolean | false;
  Authorization: string;
  XSRF_TOKEN: string;

  constructor(isAuthorized: boolean, Authorization: string, XSRF_TOKEN: string) {
    this.isAuthorized = isAuthorized || false;
    this.Authorization = Authorization || '';
    this.XSRF_TOKEN = XSRF_TOKEN || '';
  }
}

// Class UserProfil
export class UserProfil {
  public id: number;
  public firstName: string;
  public lastName: string;
  public email: string;
  public password: string;
  public role: string;

  constructor(id: number, firstName: string, lastName: string, email: string, password: string, role: string) {
    this.id = id || 0;
    this.firstName = firstName || '';
    this.lastName = lastName || '';
    this.email = email || '';
    this.password = password || '';
    this.role = role || '';
  }
}

