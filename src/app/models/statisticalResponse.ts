import { BalanceTampingStatDTO } from 'src/app/models/balanceTampingStatDTO'
import { PercentageCatStatDTO } from 'src/app/models/percentageCatStatDTO';
import { NbTransactionStatDTO } from './nbTransactionStatDTO';


export interface StatisticalResponse {
  balanceTampingStatDTOs: BalanceTampingStatDTO[];
  percentageCatStatDTOs: PercentageCatStatDTO[];
  nbTransactionStatDTOs: NbTransactionStatDTO[];
}
