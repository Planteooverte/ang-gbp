export class ChangePasswordData{
  //Property declaration
  public email : string;
  public password: string;
  public secretCode : string;
  public typeProcess : string;

  //Preparation contructor
  constructor(  email?: string, password?: string,
                secretCode?: string, typeProcess?: string){
    this.email = email || '';
    this.password = password || '';
    this.secretCode = secretCode || '';
    this.typeProcess = typeProcess || '';
  }
}
