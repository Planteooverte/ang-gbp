import { BalanceTampingStatDTO } from "./balanceTampingStatDTO";
import { NbTransactionStatDTO } from "./nbTransactionStatDTO";
import { PercentageCatStatDTO } from "./percentageCatStatDTO";

export class StatBankTransaction {
  monthDisplay: string;
  periodDisplay: string;
  balanceDisplay: number | string;
  monthCreditDisplay: number | string;
  monthDebitDisplay: number | string;
  filteredBalanceTampingStatDTOs: BalanceTampingStatDTO[];
  filteredPercentageCatDTOs: PercentageCatStatDTO[];
  filteredNbTransactionStatDTOs: NbTransactionStatDTO[];
  serialDataForBasicAreaChart: number[];
  serialDataForSemiCircleChartCredit: [string, number][];
  serialDataForSemiCircleChartDebit: [string, number][];
  listStatusBankingTransaction: any[];
  months: string [];
  monthMap: { [key: string]: number };

  constructor() {
    this.monthDisplay = '';
    this.periodDisplay = '';
    this.balanceDisplay = 'N/A';
    this.monthCreditDisplay = 'N/A';
    this.monthDebitDisplay = 'N/A';
    this.filteredBalanceTampingStatDTOs = [];
    this.filteredPercentageCatDTOs = [];
    this.filteredNbTransactionStatDTOs = [];
    this.serialDataForBasicAreaChart = [];
    this.serialDataForSemiCircleChartCredit = [];
    this.serialDataForSemiCircleChartDebit = [];
    this.listStatusBankingTransaction = [];
    this.months = [];
    this.monthMap = {};
  }
}
