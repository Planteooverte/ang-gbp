import { TypeAccount } from "./bankAccount";

export class LongCsvFile{
  //Déclaration des propriétés
  public id: number;
  public fileName: string;
  public creationDate: string;
  public updateDate: string;
  public referenceAccount: string;
  public typeAccount: string;
  public bankName: string;
  public bankAccountId: number;

  //Préparation du constructeur
  constructor(id?: number, fileName?: string, creationDate?: string,
              updateDate?: string, referenceAccount?: string,
              typeAccount?: TypeAccount, bankName?: string,
              bankAccountId?: number){
    this.id = id || 0;
    this.fileName = fileName || '';
    this.creationDate = creationDate || '';
    this.updateDate = updateDate || '';
    this.referenceAccount = referenceAccount || '';
    this.typeAccount = typeAccount || TypeAccount.Non_defini;
    this.bankName = bankName || '';
    this.bankAccountId = bankAccountId || 0;
  }
}
