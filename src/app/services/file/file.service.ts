import { Injectable } from '@angular/core';
import * as XLSX from 'xlsx';

@Injectable({
  providedIn: 'root'
})
export class FileService {

  constructor() { }

  //////////////////////////////////METHODE PREPARING FILECONTENT//////////////////////////////////
  readFileContent(file: File): Promise<any[]> {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.onload = async (event: any) => {
        try {
           if (file.name.endsWith('.csv')) {
            const csvData = event.target.result;
            if (typeof csvData === 'string') {
              const transactions = this.processCsvData(csvData);
              resolve(transactions);
            } else {
              reject("File read error: CSV data is not a string.");
            }
          }
          else if (file.name.endsWith('.xlsx')) {
            const data = new Uint8Array(event.target.result);
            const workbook = XLSX.read(data, { type: 'array' });
            const transactions: any[] = [];
            workbook.SheetNames.forEach(sheetName => {
              const worksheet = workbook.Sheets[sheetName];
              const jsonData = XLSX.utils.sheet_to_json(worksheet, { header: 1 });
              transactions.push(...this.processXlsxData(jsonData));
            });
            resolve(transactions);
          } else {
            reject("File read error: Unsupported file type.");
          }
        } catch (error) {
          reject("File read error:" + error);
        }
      };
      reader.onerror = (error) => {
        reject("File read error:" + error);
      };
      if (file.name.endsWith('.csv')) {
        reader.readAsText(file); // Lire le contenu du fichier CSV en tant que texte
      } else {
        reader.readAsArrayBuffer(file);
      }
    });
  }

  //////////////////////////////////METHODES READ AND PARSE EXCEL FILE//////////////////////////////////
  processXlsxData(data: any[]): any[] {
    return data.slice(1).map((row: any[]) => ({
      date: this.excelDateToJSDate(row[0]),
      description: row[1],
      debit: row[2] ? parseFloat(row[2]) : null,
      credit: row[3] ? parseFloat(row[3]) : null
    }));
  }

  processCsvData(csvData: string): any[] {
    const rows = csvData.split('\n').filter(row => row.trim() !== '');
    return rows.slice(1).map((row: string) => {
      const cols = row.split(';').map(col => col.trim());
      return {
        date: this.formatDate(cols[0]),
        description: cols[1],
        debit: cols[2] ? parseFloat(cols[2]) : null,
        credit: cols[3] ? parseFloat(cols[3]) : null
      };
    });
  }

  formatDate(dateString: string): string {
    const [day, month, year] = dateString.split('/').map(part => parseInt(part, 10));
    return `${year}-${this.padZero(month)}-${this.padZero(day)}`;
  }

  excelDateToJSDate(serial: number): string {
    const utc_days = Math.floor(serial - 25569);
    const utc_value = utc_days * 86400;
    const date_info = new Date(utc_value * 1000);
    return `${date_info.getFullYear()}-${this.padZero(date_info.getMonth() + 1)}-${this.padZero(date_info.getDate())}`;
  }

  padZero(num: number): string {
    return num < 10 ? '0' + num : num.toString();
  }

}
