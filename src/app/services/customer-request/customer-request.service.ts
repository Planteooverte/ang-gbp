import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { endpoints } from 'src/environments/endpoints';
import { environment } from 'src/environments/environment';
import { RegistrationData } from 'src/app/models/registrationData';
import { ChangePasswordData } from 'src/app/models/changePasswordData';


@Injectable({
  providedIn: 'root'
})
export class CustomerRequestService {

  constructor(private http: HttpClient) {

  }

  ////////////////////////////////////////HTTP METHOD MANAGEMENT CUSTOMER/////////////////////////////////////////////////////////
  getCustomer(credentials: { email: string, password: string }){
    console.log("Request - GET Profil for user:", credentials.email);
    return this.http.get(environment.backurl + endpoints.CONNECT_URL + "?email=" + credentials.email, { observe: 'response', withCredentials: true });
  }

  getSecretCode(email: string, typeProcess: string){
    console.log("Request - GET Need secretCode send to email:", email, "with process type:", typeProcess);
    return this.http.get(environment.backurl + endpoints.SECRET_CODE_URL + "?email=" + email + "&typeProcess=" + typeProcess, { observe: 'response', responseType: 'text' });
  }

  addRegistrationWithSecretCode(registrationData: RegistrationData){
    console.log("Request - POST RegistrationData send", registrationData);
    return this.http.post(environment.backurl + endpoints.REGISTRATION_NEW_USER_URL, registrationData, { observe: 'response', responseType: 'text' });
  }

  updatePwdWithSecretCode(changePasswordData: ChangePasswordData){
    console.log("Request - UPDATE Forgotten password:", changePasswordData);
    return this.http.put(environment.backurl + endpoints.FORGOTTEN_PASSWORD_URL, changePasswordData, { observe: 'response', responseType: 'text' });
  }

  deleteJWTforUser(userId: number){
    console.log("Request - DELETE Deconnexion user with ID:", userId);
    return this.http.delete(environment.backurl + endpoints.DISCONNECT_URL, { observe: 'response', withCredentials: true, responseType: 'text' });
  }

  updateProfilForUser(userId: number, form: any){
    console.log("Request - UPDATE Profil for userId:", userId);
    return this.http.put(environment.backurl + endpoints.UPDATE_PROFIL_URL, form, { observe: 'response', withCredentials: true, responseType: 'text' });
  }

  deleteUser(userId: number){
    console.log("Request - DELETE Profil for userId:", userId);
    return this.http.delete(environment.backurl + endpoints.DELETE_USER_URL, { observe: 'response', withCredentials: true, responseType: 'text' });
  }

}

// # Ce programme est un logiciel libre : vous pouvez le redistribuer et/ou le modifier
// # selon les termes de la Licence Publique Générale GNU publiée par la
// # Free Software Foundation, version 3.

// # Ce programme est distribué dans l'espoir qu'il sera utile,
// # mais SANS AUCUNE GARANTIE ; sans même la garantie implicite de
// # COMMERCIALISATION ou d'ADÉQUATION À UN OBJECTIF PARTICULIER. Voir la
// # Licence Publique Générale GNU pour plus de détails.

// # Vous devriez avoir reçu une copie de la Licence Publique Générale GNU
// # avec ce programme. Si ce n'est pas le cas, voir https://www.gnu.org/licenses/.

// # Conditions supplémentaires :
// # 1. Le logiciel peut être utilisé et modifié uniquement dans un cadre personnel ou éducatif (enseignement, apprentissage).
// # 2. Toute redistribution du logiciel dans un but professionnel est interdite.
// # 3. La commercialisation du logiciel est interdite sans l'accord explicite de l'auteur.
// # 4. Toute version modifiée du logiciel utilisée dans un cadre commercial doit rendre le code source disponible.
