import { TestBed } from '@angular/core/testing';

import { FinancialRequestService } from './financial-request.service';

describe('FinancialRequestService', () => {
  let service: FinancialRequestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FinancialRequestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
