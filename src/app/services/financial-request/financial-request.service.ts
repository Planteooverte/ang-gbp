import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { endpoints } from 'src/environments/endpoints';
import { Observable } from 'rxjs/internal/Observable';
import { StatisticalResponse } from 'src/app/models/statisticalResponse';

@Injectable({
  providedIn: 'root'
})
export class FinancialRequestService {

  constructor(private http:HttpClient) {}

  /////////////////////////////////////////////////////HTTP METHOD BANKACCOUNT/////////////////////////////////////////////////////////
  getShortListBankAccountsByUserId(id: number) {
    console.log("Request - GET BankAccount for USERID:", id);
    return this.http.get(environment.backurl + endpoints.LISTBANKACCOUNT_URL + "?id="+id, { observe: 'response', withCredentials: true });
  }

  getBankAccountsByUserId(id: number) {
    console.log("Request - GET BankAccount for USERID:", id);
    return this.http.get(environment.backurl + endpoints.BANKACCOUNT_URL + "?id="+id, { observe: 'response', withCredentials: true });
  }

  addBankAccount(form: any){
    console.log("Request - CREATE BankAccount with DATA:", form);
    return this.http.post(environment.backurl + endpoints.BANKACCOUNT_URL, form, { observe: 'response', withCredentials: true });
  }

  updateBankAccount(form:any){
    console.log("Request - UPDATE BankAccount with DATA:", form);
    return this.http.put(environment.backurl + endpoints.BANKACCOUNT_URL, form, { observe: 'response', withCredentials: true });
  }

  deleteBankAccount(id: number){
    console.log("Request - DELETE BankAccount with ID:", id);
    return this.http.delete(`${environment.backurl + endpoints.BANKACCOUNT_URL}/${id}`, { observe: 'response', withCredentials: true });
  }

  /////////////////////////////////////////////////////HTTP METHOD CSVFILE/////////////////////////////////////////////////////////
  getCsvFilesByUserId(id:number) {
    console.log("Request - GET CsvFile for USERID:", id);
    return this.http.get(environment.backurl + endpoints.CSVFILE_URL + "?id="+id, { observe: 'response', withCredentials: true });
  }

  addCsvFile(payloadCsvFile: any) {
    console.log("Request - CREATE CsvFile with DATA:", payloadCsvFile);
    return this.http.post(environment.backurl + endpoints.CSVFILE_URL, payloadCsvFile, { observe: 'response', withCredentials: true });
  }

  updateCsvFile(payloadCsvFile: any, options?: any) {
    console.log("Request - UPDATE CsvFile with DATA:", payloadCsvFile);
    options = options || {}; // Utiliser les options fournies ou initialiser un objet vide
    options = { ...options, responseType: 'text' as 'json', observe: 'response', withCredentials: true };
    return this.http.put(environment.backurl + endpoints.CSVFILE_URL, payloadCsvFile, options);
  }

  deleteCsvFile(id: number){
    console.log("Request - DELETE CsvFile with ID:", id);
    return this.http.delete(`${environment.backurl + endpoints.CSVFILE_URL}/${id}`, { observe: 'response', withCredentials: true });
  }

  /////////////////////////////////////////////////HTTP METHOD BANKINGTRANSACTION//////////////////////////////////////////////////
  getBankingTransactionByBankAccountId(id:number) {
    console.log("Request - GET BankingTransaction for BANKACCOUNT:", id);
    return this.http.get(environment.backurl + endpoints.BANKINGTRANSACTION_URL + "?id="+id, { observe: 'response', withCredentials: true });
  }

  addBankingTransaction(payloadBankingTransaction: any[], options?: any) {
    console.log("Request - CREATE BankingTransaction with DATA:", payloadBankingTransaction);
    options = options || {}; // Utiliser les options fournies ou initialiser un objet vide
    options = { ...options, responseType: 'text' as 'json', observe: 'response', withCredentials: true };
    return this.http.post<any>(environment.backurl + endpoints.BANKINGTRANSACTION_URL, payloadBankingTransaction, options);
  }

  updateBankingTransactionOnCategoryId(payloadPartialBankingTransaction: any, options?: any) {
    console.log("Request - UPDATE CsvFile with DATA:", payloadPartialBankingTransaction);
    options = options || {}; // Utiliser les options fournies ou initialiser un objet vide
    options = { ...options, responseType: 'text' as 'json', observe: 'response', withCredentials: true };
    return this.http.put(environment.backurl + endpoints.BANKINGTRANSACTION_URL, payloadPartialBankingTransaction, options);
  }

  /////////////////////////////////////////////////HTTP METHOD CATEGORY/////////////////////////////////////////////////////////
  getCategory() {
    console.log("Request - GET Category:");
    return this.http.get(environment.backurl + endpoints.CATEGORY_URL, { observe: 'response', withCredentials: true });
  }

  /////////////////////////////////////////////////HTTP METHOD STATISTICAL/////////////////////////////////////////////////////////
  getStatisticalOnBankAccount(id:number): Observable<HttpResponse<StatisticalResponse>> {
    console.log("Request - GET Statistic for BANKACCOUNT:", id);
    return this.http.get<StatisticalResponse>(environment.backurl + endpoints.STATS_URL + "?id="+id, { observe: 'response', withCredentials: true });
  }

}

// # Ce programme est un logiciel libre : vous pouvez le redistribuer et/ou le modifier
// # selon les termes de la Licence Publique Générale GNU publiée par la
// # Free Software Foundation, version 3.

// # Ce programme est distribué dans l'espoir qu'il sera utile,
// # mais SANS AUCUNE GARANTIE ; sans même la garantie implicite de
// # COMMERCIALISATION ou d'ADÉQUATION À UN OBJECTIF PARTICULIER. Voir la
// # Licence Publique Générale GNU pour plus de détails.

// # Vous devriez avoir reçu une copie de la Licence Publique Générale GNU
// # avec ce programme. Si ce n'est pas le cas, voir https://www.gnu.org/licenses/.

// # Conditions supplémentaires :
// # 1. Le logiciel peut être utilisé et modifié uniquement dans un cadre personnel ou éducatif (enseignement, apprentissage).
// # 2. Toute redistribution du logiciel dans un but professionnel est interdite.
// # 3. La commercialisation du logiciel est interdite sans l'accord explicite de l'auteur.
// # 4. Toute version modifiée du logiciel utilisée dans un cadre commercial doit rendre le code source disponible.
