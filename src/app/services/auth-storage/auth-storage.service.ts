import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AuthStorageService {
  private email: string | null = null;
  private password: string | null = null;

  setCredentials(email: string, password: string) {
    this.email = email;
    this.password = password;
  }

  clearCredentials() {
    this.email = null;
    this.password = null;
  }

  getEmail() {
    return this.email;
  }

  getPassword() {
    return this.password;
  }
}
