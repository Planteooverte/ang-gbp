import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { UserDataStorageService } from 'src/app/services/user-storage/user-data-storage.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router,
              private userDataStorageService: UserDataStorageService
            ) {}

  canActivate(): boolean {
    const userData = this.userDataStorageService.getUserData();
    // L'utilisateur est connecté, on renvoie TRUE pour autorisé l'accès à la page
    if (userData && userData.userSecurity && userData.userSecurity.Authorization) {
      return true;
    }
    // L'utilisateur n'est pas connecté, on renvoie FALSE pour bloquer l'accès à la page
    else {
      return false;
    }
  }
}
