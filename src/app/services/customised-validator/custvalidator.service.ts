import { Injectable } from '@angular/core';
import { AbstractControl, ValidationErrors } from '@angular/forms';

const MAX_FILE_SIZE = 100 * 1024; // 100 Kilo-octets
const VALID_EXTENSIONS = ['.csv', '.xlsx'];

@Injectable({
  providedIn: 'root'
})
export class CustvalidatorService {

  constructor() { }

  //////////////////////////////////CUSTOM VALIDATORS//////////////////////////////////
  fileNameValidator(control: AbstractControl): ValidationErrors | null {
    const fileName = control.value;
    if (!fileName) {
      return null;
    }
    const fileExtension = fileName.slice((fileName.lastIndexOf('.') - 1 >>> 0) + 2);
    if (!VALID_EXTENSIONS.includes('.' + fileExtension)) {
      return { invalidFileExtension: true };
    }
    return null;
  }

  fileSizeValidator(control: AbstractControl): ValidationErrors | null {
    const file = control.value as File;
    if (file instanceof File) {
      if (file.size > MAX_FILE_SIZE) {
        return { invalidFileSize: true };
      }
    }
    return null;
  }
}
