import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { UserInterface, UserPreference, UserSecurity, UserProfil } from 'src/app/models/userInterface';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserDataStorageService {

  private userData: UserInterface | null = null;
  private isAuthorizedSubject = new BehaviorSubject<boolean>(false);

  constructor() {}

  // Method for obtaining user data
  getUserData(): UserInterface | null {
    return this.userData;
  }

  // Method for defining user data
  setUserData(userData: UserInterface): void {
    this.userData = userData;
    this.isAuthorizedSubject.next(userData.userSecurity.isAuthorized);

    // If the environment is in ‘Dev’ mode, save a copy in the sessionStorage
    if (environment.mode === 'Dev') {
      window.sessionStorage.setItem('userPreference', JSON.stringify(userData.userPreference));
      window.sessionStorage.setItem('userSecurityData', JSON.stringify(userData.userSecurity));
      window.sessionStorage.setItem('userProfil', JSON.stringify(userData.userProfil));
    }
  }

  // Method for deleting user data
  clearUserData(): void {
    if (this.userData) {
      this.userData.userPreference = new UserPreference('Defaut', 'Defaut', 'Defaut');
      this.userData.userSecurity = new UserSecurity(false, '', '');
      this.userData.userProfil = new UserProfil(0, '', '', '', '', '');
      this.isAuthorizedSubject.next(false);

      // If the environment is in ‘Dev’ mode, delete the data from the sessionStorage
      if (environment.mode === 'Dev') {
        window.sessionStorage.removeItem('userPreference');
        window.sessionStorage.removeItem('userSecurityData');
        window.sessionStorage.removeItem('userProfil');
      }
    }
  }

  // Method for obtaining an Observable on isAuthorized
  getIsAuthorized$() {
    return this.isAuthorizedSubject.asObservable();
  }

  // Method for updating authorisation status
  setAuthorized(isAuthorized: boolean) {
    if (this.userData) {
      this.userData.userSecurity.isAuthorized = isAuthorized;
      this.isAuthorizedSubject.next(isAuthorized);
    }
  }

  // Method for updating userProfil only
  updateUserProfil(userProfil: UserProfil): void {
    if (this.userData) {
      this.userData.userProfil = userProfil;

      // If the environment is in ‘Dev’ mode, save the update in the sessionStorage
      if (environment.mode === 'Dev') {
        window.sessionStorage.setItem('userProfil', JSON.stringify(this.userData.userProfil));
      }
    }
  }

  // Method for deleting user profil
  clearUserProfil(): void {
    if (this.userData) {
      this.userData.userProfil = new UserProfil(0, '', '', '', '', '');

      // If the environment is in ‘Dev’ mode, delete the data from the sessionStorage
      if (environment.mode === 'Dev') {
        window.sessionStorage.removeItem('userProfil');
      }
    }
  }

  // Method for deleting user data
  clearUserSecurity(): void {
    if (this.userData) {
      this.userData.userSecurity = new UserSecurity(false, '', '');
      this.isAuthorizedSubject.next(false);

      // If the environment is in ‘Dev’ mode, delete the data from the sessionStorage
      if (environment.mode === 'Dev') {
        window.sessionStorage.removeItem('userSecurityData');
      }
    }
  }

  // Method for updating X-CSRF-TOKEN
  updateXSRFToken(newToken: string): void {
    if (this.userData) {
      this.userData.userSecurity.XSRF_TOKEN = newToken;

      // If the environment is in ‘Dev’ mode, update the X-CSRF-TOKEN in the sessionStorage
      if (environment.mode === 'Dev') {
        const userSecurityData = JSON.parse(window.sessionStorage.getItem('userSecurityData') || '{}');
        userSecurityData.XSRF_TOKEN = newToken;
        window.sessionStorage.setItem('userSecurityData', JSON.stringify(userSecurityData));
      }
    }
    //console.log("XSRF_TOKEN got from header", newToken);
    //console.log("XSRF_TOKEN saved in userDataStorage", this.userData?.userSecurity.XSRF_TOKEN)
  }

}
