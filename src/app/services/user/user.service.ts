import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { userPreferenceInterface } from 'src/app/models/userSession';

@Injectable({
  providedIn: 'root'
})

export class UserService {
  private userPreferencesItem = new BehaviorSubject<userPreferenceInterface>({
    contrast: 'Défaut',
    font: 'Défaut',
    lineSpacing: 'Défaut',
  });

  //Déclaration d'un observable (userPreferences$) pour abonner les composants afin d'avoir les mises à jour des préférences utilisateur.
  userPreferences$ = this.userPreferencesItem.asObservable();

  updateUserPreferences(newPreferences: userPreferenceInterface){
    console.log(newPreferences);
    this.userPreferencesItem.next(newPreferences);
  }

  getUserPreferences() {
    return this.userPreferencesItem.value;
  }
}
