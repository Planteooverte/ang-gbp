import { enableProdMode } from "@angular/core";
import { environment } from "./environment";

if (environment.mode == 'Prod') {
  enableProdMode();
  if (window) {
    window.console.log= function(){};
  }
};
