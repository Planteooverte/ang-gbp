export const environment = {
  mode: 'Dev' ,                       //Dev or Prod
  backurl: 'http://localhost:8080',   //URL Sprint boot
  secretKey: '7774010B5615E1A4',      //Clef encryption AES
  countdown: 1,                       //Timer fenêtre message success
  MAX_FILE_SIZE: 100 * 1024,          //Taille fichier chargé de 100 Kilo-octets
};
